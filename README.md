Fork: Introduced poetry for easier dependency management and reproducability.
Simply install poetry (e.g. by `pip install poetry`), then add your netsquid credentials, by
```bash
poetry config http-basic.netsquid-pypi <netsquid-username> <netsquid-password>
```
On headless systems (e.g. when connecting to a cluster via ssh) you will need to execute `keyring --disable` first.

Finally:
```bash
poetry install
```

# Simulation Code for "Requirements for a processing-node quantum repeater on a real-world fiber grid"

This repository contains the code required to reproduce the results from the paper
"Requirements for a processing-node quantum repeater on a real-world fiber grid"
by Guus Avis, Francisco Ferreira da Silva, Tim Coopmans, Axel Dahlberg, Hana Jirovská, David Maier, Julian Rabbie, Ariana Torres-Knoop and Stephanie Wehner.
The code has been developed by Guus Avis, Francisco Ferreira da Silva, Tim Coopmans, Axel Dahlberg, Hana Jirovská, David Maier and Julian Rabbie.

We first describe how these simulations could be adapted to simulate other types of hardware.
This is followed by a general description of how simulations can be executed using the code in this repository.
Finally, this file contains instructions for how to specifically use the code to reproduce the results from the paper.

**Note**
Much of the code required to run simulations of (chains of) quantum repeaters that is included in this code repository has later been released as a separate, dedicated software package named [NetSquid-QRepChain](https://gitlab.com/softwarequtech/netsquid-snippets/netsquid-qrepchain).
Even though we below explain how the simulations included in this repository could be adapted, we encourage anyone looking to set up their own quantum-repeater simulations to use that package instead of the code included here.
The reason for this is that that package is actively maintained and allows for easier user interaction and contribution.
The main purpose of this code repository, on the other hand, is to allow for the reproduction of results in the paper.


How to adapt the simulations
----------------------------

In the paper to which this code repository corresponds, we run quantum-repeater simulations to determine minimal requirements for three types of processing-node quantum repeaters.
These are color centers, ion traps and "abstract nodes".
If one aims to adapt our simulations to study different kinds of processing nodes, there are two ways of going about this, namely an easy way using the abstract nodes and a harder way that involves building new models.

## 1 Easy way

The easy way to adapt the simulations is by using the "abstract-node" simulations included here.
These abstract nodes provide a simple and somewhat hardware-agnostic model for processing nodes.
One can map the parameters of the processing-node device one wants to investigate to abstract-node parameters, and then use those parameters to run abstract-node simulations.
For more information on the abstract node, see [NetSquid-AbstractModel](https://gitlab.com/softwarequtech/netsquid-snippets/netsquid-abstractmodel).
For discussion about how well the abstract node is able to represent other types of processing nodes (specifically color centers and ion traps), see the paper.
For examples of how to map the parameters of one processing node to the abstract node, see the files in the folder `nlblueprint/abstract_node_mapping`.
Conretely, to simulate the one-repeater setup investigated in the paper (assuming the double-click protocol), one should first put the proper parameters in `simulations/standard_configs/abstract_baseline_params.yaml`, and then run (from the `simulations` folder)

`python3 unified_simulation_script_state.py standard_configs/delft_eindhoven_with_asym_config_abstract_double_click.yaml -pf standard_configs/abstract_baseline_params.yaml`

See the "How to run simulations" sections below for more detail on how to run simulations, and "How to reproduce results from the paper" for how to use them to find minimal hardware parameters.
Note that one can only run simulations in which entanglement is generated using either the single-click or double-click protocol this way.

## 2 Hard way

If one wants to run simulations for some type of processing node at a level of detail unavailable using the abstract node, or simulate a type of entanglement generation that is not the single- or double-click protocol, new models must be created.
This requires both physical modeling and translating these models into NetSquid code.
For examples on how to make a NetSquid `QuantumProcessor` corresponding to a specific processing node, see [NetSquid-NV](https://gitlab.com/softwarequtech/netsquid-snippets/netsquid-nv) and [NetSquid-TrappedIons](https://gitlab.com/softwarequtech/netsquid-snippets/netsquid-trappedions).
For information on how to make a `MagicDistributor` that simulates a specific type of entanglement generation, see [NetSquid-Magic](https://gitlab.com/softwarequtech/netsquid-snippets/netsquid-magic).

To then use such models to run the simulations included here, one needs to:
1. Make nodes with drivers.
A driver is a NetSquid component that allows protocols at a node to interface with one another, see [NetSquid-Driver](https://gitlab.com/softwarequtech/netsquid-snippets/netsquid-driver).
The nodes need to have a driver installed with the proper services defined.
We recommend reverse engineering from the file `nlblueprint/processing_nodes/nodes_with_drivers.py`.
2. Add the node with driver and the magic distributor to `simulations/unified_simulation_script_state.py`.
The magic distributor needs to be included with a given name in `distributor_name_to_class`, the node needs to be added to `ComponentBuilder` with a name in the `setup_networks` function.
3. Adapt YAML configuration files.
The YAML configuration files need to be adapted to use the new magic distributor and the new node with driver.
More specifically, the `type` of each node needs to be set to the name under which the new node type was registered to `ComponentBuilder`,
the `distributor` of each entangling connection (those that have type `heralded_connection`) needs to be set to the name under which the magic distributor was added to `distributor_name_to_class`,
and the parameters passed under `properties` for each node and entangling connection need to be set in correspondance with the parameters requires by the newly-implemented models.
For instance, one could adapt `simulations/standard_configs/delft_eindhoven_with_asym_config_abstract_double_click.yaml` by everywhere replacing `abstract_node` by the new node name, `double_click` by the new magic distributor name, and replacing the reference to `abstract_baseline_params.yaml` on line 14 by a reference to a new YAML parameter file that lists all the parameter values required by the used models.
See [NetSquid-NetConf](https://gitlab.com/softwarequtech/netsquid-snippets/netsquid-netconf) for more details about how to set up networks using YAML configuration files.

Then, one can run the new configuration file using either of the unified simulation scripts per the instructions below.


How to run simulations
----------------------
## 1 Installation 

Before running simulations, perform the following steps.

Create a virtual environment:

`mkdir my_virtual_environment`

`python3 -m venv my_virtual_environment`

`source my_virtual_environment/bin/activate`

Start by adding this repository to your python path:

`export PYTHONPATH=$PYTHONPATH:<fill-this-path>`

(This only sets this variable for your current session. To make this change permanent add this line to your .bashrc file instead.)

In order to be able to install all the snippets from the netsquid pypi servers, as a next step add your Netsquid credentials as environment variables:

`export NETSQUIDPYPI_USER=<netsquid-forum-username>`

`export NETSQUIDPYPI_PWD=<netsquid-forum-password>`

Your Netsquid credentials are the same as on the forum (https://forum.netsquid.org/).

(Again, this only sets these variables for your current session. To make this change permanent add both lines to your .bashrc file instead. After saving changes to your .bashrc file, don't forget to `source .bashrc` (or restart your terminal session).)

Now, from within the repository, install all necessary dependencies by executing:

`make python-deps`

If necessary, enter your NetSquid credentials again when prompted.
(This will automatically install a NetSquid version compatible with all our code)
Should this step produce any more errors about permissions, please contact David M. (currently the responsible Netsquid forum maintainer) to check the user group of your Netsquid forum account and adjust it if necessary.

You can verify your installation by running the unittests as:

`make tests` (or `make tests_full` to also run integration tests)

## 2 Simulation

All our repeater chain simulations can be run with our unified simulation script:

`simulations/unified_simulation_script_state.py`

(There is also a `unified_simulation_script_qkd.py` that simulates QKD rather than entanglement distribution.)

All that is necessary to perform a simulation are two additional files:

* `config_file.yaml` Network configuration file, for more details check out the [Netconf-snippet.](https://gitlab.com/softwarequtech/netsquid-snippets/netsquid-netconf)
* `parameter_file.yaml` List of all simulation parameters. (Needs to be linked at the beginning of the config_file.yaml, see examples)

Examples can be found in:

* (NV-centers) `examples/nv_config.yaml` `examples/nv_sim_params.yaml`
* (Trapped Ions) `examples/ti_config.yaml` `examples/ti_sim_params.yaml`
* (Abstract) `examples/abstract_config.yaml` `examples/abstract_sim_params.yaml`

The simulation is then executed as:

`python3 unified_simulation_script_state.py config_file.yaml -pf parameter_file.yaml`

See further available input arguments by

`python unified_simulation_script_state.py -h`


## 3 Data Processing and Plotting

By making use of `netsquid-netconf`'s one can perform parameter sweeps using the unified simulation script and directly plot the results. These are some example configuration files implementing parameter sweeps:

* (NV-centers) `examples/nv_config_vary_t2.yaml` 
* (Trapped Ions) `examples/ti_config_vary_coherence.yaml` 
* (Abstract) `examples/abstract_config_vary_t2.yaml` 

The simulation is in this case executed as:

`python3 unified_simulation_script_state.py config_file.yaml --plot`

The `--plot` flag ensures that the results will be plotted directly after the simulation terminates. Alternatively, one can run the simulation without the `--plot` flag and instead plot the results later using `simulations/plot_unified_state.py` on the simulation's output file (default: `output.csv`) (`plot_unified_qkd.py` for data generated by QKD simulations).

Besides the above-described functionality, we also use the public snippet `netsquid-simulationtools` for data processing and plotting.

We suggest cloning and installing it via:

`git clone https://gitlab.com/softwarequtech/netsquid-snippets/netsquid-simulationtools.git`

`cd netsquid-simulationtools`

`make install`

Verify your installation again with `make tests`

The simulation data can now be processed using

`netsquid_simulationtools/repchain_data_process.py`

and plotted with

`netsquid_simulationtools/repchain_data_plot.py`

Edit the plotting command/script as needed locally.


How to reproduce results from the paper
---------------------------------------
## Finding minimal hardware requirements

In order to find minimal hardware requirements, the genetic-algorithm optimization methodology introduced in the paper must be employed.
An example of the files required to run it can be found in the `simulations/minimal_hardware_requirements/move_double_click_surf_color/src` folder.
In particular, the files there can be used to find the minimal hardware requirements to achieve an entanglement generation rate of 0.5 Hz and a teleportation fidelity of 0.8571 for a one-repeater setup connecting Delft and Eindhoven in SURF's fiber grid using color centers that perform double-click entanglement generation, with end nodes that map their half of entangled states to a memory qubit once entanglement is generated.
These files are:

 - `input_file.ini` - Defines choice of hyperparameters for genetic algorithm (including number of generations, mutation and crossover probabilities, population size and number of parents; for more details, see arXiv:2010.16373), parameters to be optimized, which scripts should be used to run the simulation and to process data and which configuration and parameter files should be passed as input to the simulation. The fields `sstoposdir` and `venvdir` define the directories in which `smart-stopos` is installed and where the virtual environment to use is defined. These should be updated to match the user's definitions. See the README in the `smart-stopos` [repository](https://gitlab.com/aritoka/smart-stopos) for details.
 - `nv_baseline_params.yaml` - `YAML` file containing the baseline parameters from which to optimize. For further details on the meaning of 'baseline parameters', see the acommpanying paper.
 - `nv_surf_config.yaml` - `YAML` file defining the network to be simulated. Contains information about node hardware, as well as length and attenuation of connection between nodes.
 - `processing_function.py` - Script used for processing of simulation data. From the simulation's output, which consists of dataframes containing density matrix and time required for entanglement generation, computes the corresponding cost function, as defined in the paper.
 - `run_local.sh`
 - `run_local_bg.sh` - This file and the above are used to execute the optimization procedure. They can both be used either locally, in a regular computer, or on a cluster that makes use of `slurm`.
 - `run_multiple_optimizations.sh` - Can be used on a cluster that makes use of `slurm` to submit multiple optimization runs simultaneously.
 - `unified_simulation_script_with_translation.py` - Layer of translation around the `unified_simulation_script.py` that is used to run the blueprint simulations. This is used to convert parameters from probabilities of no-error to their actual values.

For example, in order to execute an optimization procedure on your machine, you might execute the command:

`./run_local.sh`.

Alternatively,

`./run_local_bg.sh`,

has the same effect, but results in the optimization running on the background. If you wish to execute the procedure on a cluster, you should instead run:

`sbatch ./run_local_bg.sh`.

If you wish to perform `n` optimization runs of the same setup, you can run:

`./run_multiple_optimizations n`.

The files used to find minimal requirements for the other setups shown in the paper (different hardware platforms, namely trapped ions and abstract nodes, different architectures, namely equally spaced and an alternative path between Delft and Eindhoven, different target metrics and different entanglement generation protocols) can be found in the `src` folder inside of the corresponding archive of <https://data.4tu.nl/articles/dataset/Replication_data_for_Requirements_for_a_processing-node_quantum_repeater_on_a_real-world_fiber_grid/19746748>.


## Finding absolute minimal requirements

Absolute minimal requirements are found by performing a sweep over the improvement factor (see paper for a definition) for each parameter while making all the other parameters except for fiber attenuation perfect. This can be done using the `.sh` files present in `/simulations/absolute_minimal_requirements`.
For example, in order to find the absolute minimal requirements for color center parameters, you should go to `/simulations/absolute_minimal_requirements` and then run:

`./nv_minimal_requirements_analysis.sh`

The output will be a `.csv` file containing the name of each parameter, the improvement factor at which the targets were met and to which parameter value this corresponded. Note that in order to obtain a more fine-grained value, you can adjust the values of the improvement factor that are being swept over by changing the numbers after `-if` in line 8 of the `.sh` file. The same holds for changing the target rate: it can be changed by changing the number after `-rt` in the same line. Note that the fidelity target is automatically computed such that the combination of rate and fidelity are such that Verified Blind Quantum Computing is possible under the assumptions laid out in the paper.

## Figures 3, 4 and 16-19: minimal hardware requirements circular plots

The scripts used to generate these plots can be found in `simulations/minimal_hardware_requirements`, under the names `circular_plot_one_setup.py` and `circular_plot_two_setups.py`. They can be used to generate a circular plot with the minimal requirements for a single setup or for two setups, respectively. `circular_plot_one_setup.py` takes the following arguments:

 - `parameters`: Parameters to be included in the plot.
 - `baseline_param_file`: `YAML` file containing baseline parameter values with respect to which the required improvements showed in the plots will be computed.
 - `solution_param_file`: `YAML` file containing minimal hardware requirement parameter values (this is the output of the optimization procedure).
 - `output_path`: Path to where plot should be stored.
 - `log`: Whether or not to use logarithmic scale in the plot.

For example, if you wanted to plot the minimal hardware requirements in file `example_requirements.yaml` with respect to the baseline `example_baseline.yaml` for parameters `example_parameter_1`, `example_parameter_2` and `example_parameter_3` you should run:

`python3 circular_plot_one_setup.py --parameters example_parameter_1 example_parameter_2 example_parameter_3 --baseline_parameter_file example_baseline.yaml --solution_param_file example_requirements.yaml --log True`

`circular_plot_two_setups.py` works identically, but requires that two minimal requirement files be passed.


## Figures 5 and 6: minimal hardware requirements bar plots

The script used to generate these plots can be found in `simulations/absolute_minimal_requirements`, under the name `bar_plot_horizontal.py`. It takes the following arguments:

 - `parameters`: Parameters to be included in the plot.
 - `solution_param_file_1`: `YAML` file containing minimal hardware requirement parameter values for the first setup you want to visualize.
 - `solution_param_file_2`: `YAML` file containing minimal hardware requirement parameter values for the second setup you want to visualize.
 - `label_1`: Label for the first setup you want to visualize.
 - `label_2`: Label for the second setup you want to visualize.

For example, if you wanted to compare the minimal hardware requirements in file `example_requirements_1.yaml` to the ones in `example_requirements_2.yaml` for parameters `example_parameter_1`, `example_parameter_2` and `example_parameter_3` you should run:

`python3 bar_plot_horizontal.py --parameters example_parameter_1 example_parameter_2 example_parameter_3 --solution_param_file_1 example_requirements_1.yaml --solution_param_file_2 example_requirements_2.yaml --label_1 "First example" --label_2 "Second example"`


## Figure 9: validation of color center double-click model

The script used to generate this plot can be found in `simulations/nv_double_click_verification`, under the name `nv_double_click_verification_simulate_and_plot.py`. In order to execute it, you should run:

`python3 nv_double_click_verification_simulate_and_plot.py heralded_config.yaml --paramfile heralded_entanglement_params.yaml --n_runs 10000`

The file `heralded_config.yaml` defines the network used in this simulation, which replicates the experiment described in `arXiv:1212.6136`. `heralded_entanglement_params.yaml` contains the respective parameters. The raw data from the experiment, to which the simulation results are compared, are stored in the remaining files in the same folder.


## Figure 10: validation of toy model for coincidence-time window

To generate this figure, first go to the `analytics` folder:

`cd nlblueprint/analytics`.

Then, run

`python3 effect_of_time_windows.py`.

The figure will then be generated in the current folder, with the file name

`meraner_frequency_converted_data_comparison.png`.


## Figures 11 and 12: validation of abstract model against platform-specific models

The script used to generate the relevant data can be found in `simulations/abstract_model_validation` under the name `validate_abstract_model.py`. It takes the following arguments:

 - `config_file_pltf_specific`: `YAML` configuration file defining the network of trapped ion or color center nodes.
 - `configfile_abs`: `YAML` configuration file defining the network of abstract nodes.
 - `paramfile`: `YAML` file containing baseline hardware parameters for the platform-specific model.
 - `platform`: Which hardware platform the abstract model should be validated against. Should be either `ti` or `nv`.
 - `entanglement_generation_protocol`: Entanglement generation protocol, can either be `single_click` or `double_click`.
 - `improvement_factors`: Values of the improvement factor for which to run the simulation.
 - `n_runs`: Number of runs to perform per configuration.
 - `output_path`: Where to save data, relative to current directory.

The raw data can then be processed and plotted using `process_and_plot_improvement_factor_teleportation.py`, which in turn takes the following arguments, in accordance with the output of the `validate_abstract_model.py` script:

 - `directory_pltf_1`: Directory holding files corresponding to one of the hardware platforms.
 - `directory_pltf_2`: Directory holding files corresponding to the other hardware platform.
 - `label_1`: Label for the first hardware platform.
 - `label_2`: Label for the second hardware platform.

The `YAML` files in this directory correspond to network setups for trapped ions, color centers and abstract nodes as well as corresponding baseline hardware parameters. They can be used in conjunction with the scripts described to generate the figures in the paper.  


## Figures 14 and 15: simulation performance

The script used to generate Figure 14 (Figure 15) can be found in `simulations/simulation_performance` under the name `runtime_analysis.py` (`runtime_analysis_cutoff.py`). They take the following arguments:

 - `configfile`: `YAML` configuration file defining the network.
 - `paramfile`: `YAML` file containing hardware parameters.
 - `n_runs`: Number of runs to perform per configuration.
 - `output_file_name`: Base name for data and plot storage.
 - `cutoffs`: Values of cut-off time for which to run the analysis. Valid only for `runtime_analysis_cutoff.py`.

The `YAML` files in this directory can be used in conjuction with the scripts described to generate the figures in the paper.


## Model for double-click entanglement generation

Expressions for the success probability and state are printed by running (from the root directory):

`python3 nlblueprint/analytics/double_click_model.py`.

The expressions however are not very compact and not the same as those in the paper, which are given by Equation (C4).
To verify that these results are the same as those in the paper, one can run

`python3 nlblueprint/analytics/verify_double_click_model_nice_way_of_writing.py`.

This script symbolically evaluates the difference between the output of `double_click_model.py` and the expressions in the paper, and asserts that the difference is equal to zero.
