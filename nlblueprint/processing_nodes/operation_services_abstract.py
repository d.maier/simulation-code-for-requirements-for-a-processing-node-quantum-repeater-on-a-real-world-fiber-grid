from netsquid_abstractmodel.programs import AbstractSwapProgram
from netsquid.qubits.ketstates import BellIndex
from nlblueprint.processing_nodes.measurement_services_processing_nodes import ProcessingNodeMeasureService, \
    ProcessingNodeSwapService


class AbstractSwapProgramWithBellIndex(AbstractSwapProgram):
    _translate = [BellIndex.PHI_PLUS,
                  BellIndex.PSI_PLUS,
                  BellIndex.PHI_MINUS,
                  BellIndex.PSI_MINUS]

    @property
    def outcome_as_netsquid_bell_index(self):
        old_bell_index = self.get_outcome_as_bell_index
        return self._translate[old_bell_index]


class AbstractMeasureService(ProcessingNodeMeasureService):
    """Service for performing a single-qubit measurement on an abstract node.

    If the node this service runs on has a `driver.Driver` as `driver` attribute (i.e. `node.driver`),
    and this service has an `services.entanglement_tracker_service.EntanglementTrackerService` registered,
    the measurement is registered at the entanglement tracker.

    Parameters
    ----------
    node : :class:`~netsquid.nodes.node.Node`
        The node this protocol runs on.
    name : str, optional.
        The name of this protocol. Default 'AbstractNodeMeasureService'.

    """
    def __init__(self, node, name="AbstractMeasureService"):
        super().__init__(node=node, name=name)


class AbstractSwapService(ProcessingNodeSwapService):
    """Service for entanglement swapping (i.e. performing a Bell-state measurement) on an abstract node.

    If the node this service runs on has a `driver.Driver` as `driver` attribute (i.e. `node.driver`),
    and this service has an `services.entanglement_tracker_service.EntanglementTrackerService` registered,
    the swap is registered at the entanglement tracker.

    If a discard is registered at the local `EntanglementTrackerService` while the swap program is being executed,
    the swap is considered to have failed, and all participating qubits are discarded (if they weren't already).

    Parameters
    ----------
    node : :class:`~netsquid.nodes.node.Node`
        The node this protocol runs on.
    name : str, optional.
        The name of this protocol. Default 'AbstractSwapService'.

    """
    def __init__(self, node, name="AbstractSwapService"):
        super().__init__(node=node, name=name, swap_prog=AbstractSwapProgramWithBellIndex)
