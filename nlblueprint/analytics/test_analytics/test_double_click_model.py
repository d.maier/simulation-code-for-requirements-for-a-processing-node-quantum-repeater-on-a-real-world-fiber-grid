from sympy import Matrix, sqrt, diag, zeros, refine, Q, conjugate, eye, S, simplify, trace
from sympy.physics.quantum.tensorproduct import TensorProduct
from nlblueprint.analytics.elementary_link_modeling import mu, prob_loss_a, prob_loss_b, prob_dc
import pytest
import unittest


@pytest.mark.integtest
class TestSimplifiedModel(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        """only import the model when the test is actually run;
        when it is imported, the calculation is also performed, which takes some time """

        from nlblueprint.analytics.double_click_model import succ_prob_number_resolving, \
            succ_prob_not_number_resolving, emission_fidelity_a, emission_fidelity_b, \
            state_different_click_number_resolving, state_same_click_number_resolving, \
            state_different_click_not_number_resolving, state_same_click_not_number_resolving, \
            prob_same_click_number_resolving, prob_different_click_number_resolving, \
            prob_same_click_not_number_resolving, prob_different_click_not_number_resolving

        cls.state_different_click_number_resolving = state_different_click_number_resolving
        cls.state_same_click_number_resolving = state_same_click_number_resolving
        cls.state_different_click_not_number_resolving = state_different_click_not_number_resolving
        cls.state_same_click_not_number_resolving = state_same_click_not_number_resolving
        cls.succ_prob_number_resolving = succ_prob_number_resolving
        cls.succ_prob_not_number_resolving = succ_prob_not_number_resolving
        cls.em_fid_a = emission_fidelity_a
        cls.em_fid_b = emission_fidelity_b
        cls.prob_same_click_number_resolving = prob_same_click_number_resolving
        cls.prob_different_click_number_resolving = prob_different_click_number_resolving
        cls.prob_same_click_not_number_resolving = prob_same_click_not_number_resolving
        cls.prob_different_click_not_number_resolving = prob_different_click_not_number_resolving

    def test_difference_same_click_different_click(self):
        """Check that same click and different click have same probability, and states are equal up to Pauli Z."""
        assert self.prob_same_click_number_resolving == self.prob_different_click_number_resolving
        assert self.prob_same_click_not_number_resolving == self.prob_different_click_not_number_resolving
        # note: only expect equality of states up to pauli z when there are no dark counts
        # the part added to the density matrix because of dark counts may behave differently
        pauli_z = TensorProduct(diag(1, -1), eye(2))
        state_different_click_number_resolving_z = pauli_z * self.state_different_click_number_resolving * pauli_z.H
        assert simplify((self.state_same_click_number_resolving -
                         state_different_click_number_resolving_z).subs(prob_dc, 0)) \
               == zeros(4)
        state_different_click_not_number_resolving_z = \
            pauli_z * self.state_different_click_not_number_resolving * pauli_z.H
        assert simplify((self.state_same_click_not_number_resolving -
                         state_different_click_not_number_resolving_z).subs(prob_dc, 0)) \
               == zeros(4)

    def test_only_nonunit_visibility(self):
        """Test case where only source of noise is imperfect photon indistinguishability.

        This simplified model is based on the model in
        Unconditional quantum teleportation between distant solid-state quantum bits,
        W.Pfaff et al, 10.1126/science.1253512

        Specifically, the model can be found in eq. S9 (bottom of page 10) in the Supplementary Material:
        http://www.sciencemag.org/content/345/6196/532/suppl/DC1

        NOTE
        ----
        The model uses parameter V = |<psi_a|psi_b>|^2, i.e. V = |mu|^2.

        """

        # expected state
        psi_min = Matrix([0, 1, -1, 0]) / sqrt(2)
        expected_min_state = abs(mu) ** 2 * psi_min * psi_min.H + (1 - abs(mu) ** 2) * diag(0, 1, 1, 0) / 2

        # state from model (assuming unit emission fidelity and real-valued mu)
        state = self.state_different_click_number_resolving.subs([(conjugate(mu), mu), (self.em_fid_a, 1),
                                                                  (self.em_fid_b, 1), (prob_loss_a, 0),
                                                                  (prob_loss_b, 0), (prob_dc, 0)])
        state = state / trace(state)  # states are unnormalized in the model
        assert (refine((state - expected_min_state), Q.positive(mu)) == zeros(4))

        # success probability should be 50% if there is only visibility and no losses;
        # it fails if both photons are in the same mode, and succeeds if they are in different modes
        succ_prob = self.succ_prob_number_resolving.subs([(self.em_fid_a, 1), (self.em_fid_b, 1), (prob_loss_a, 0),
                                                          (prob_loss_b, 0), (prob_dc, 0)])
        assert succ_prob == 1 / 2

    def test_minimal_emission_fidelity(self):
        """Assert that when emitting maximally mixed states, the result is maximally mixed."""
        state = self.state_different_click_number_resolving.subs([(self.em_fid_a, S(1) / 4), (self.em_fid_b, S(1) / 4)])
        state = state / trace(state)
        assert simplify(state) == eye(4) / 4

    def test_photons_always_lost(self):
        """Assert that when photons always get lost, the success probability is zero in absence of dark counts."""
        assert self.succ_prob_number_resolving.subs([(prob_loss_a, 1), (prob_loss_b, 1), (prob_dc, 0)]) == 0
        assert self.succ_prob_not_number_resolving.subs([(prob_loss_a, 1), (prob_loss_b, 1), (prob_dc, 0)]) == 0

    def test_always_dark_counts(self):
        """Assert that the success probability is zero if there are always dark counts."""
        assert self.succ_prob_number_resolving.subs(prob_dc, 1) == 0
        assert self.succ_prob_not_number_resolving.subs(prob_dc, 1) == 0

    def test_verify_nice_writing(self):
        """Verify that the nice way of writing the analytical model is correct."""
        import nlblueprint.analytics.verify_double_click_model_nice_way_of_writing as verify
        verify
