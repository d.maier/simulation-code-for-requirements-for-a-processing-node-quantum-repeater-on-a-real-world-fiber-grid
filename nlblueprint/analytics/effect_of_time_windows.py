import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy.optimize import least_squares


"""
This code computes relevant quantities for double-click heralded entanglement distribution,
based on a simplified model.
In these schemes, there is a detection time window.
Photons detected outside of this detection time window are not registered, leading to heralded failure.
Additionally, there is a coincidence time window.
If the time difference between two photon detections is larger than the coincidence time window,
the event is heralded as a failure.
Imposing a small coincidence window improves the visibility and therefore the elementary-link fidelity,
but comes at the cost of a lower rate.

The code in this file allows for the calculation of the coincidence probability and visibility as a function
of the coincidence time window and detection time window using a simplified model.
The model is based on two assumptions:
1. Each emitter emits pure photons (i.e. a photon in a pure state) at a random time.
   The emission-time probability density function is exponential.
2. Each pure photon has an exponential wave function.
A derivation of this model will be included in the blueprint docs repo.

Additionally, the code in this file can be used to fit the parameters of the two exponential distributions
to experimental data.

"""


plt.rcParams.update({'font.size': 12})


def decay_param_from_half_life(half_life_time):
    """Convert exponential decay parameter to a half-life time.

    :math:`e ^ {- \\text{decay\\_param} * t} = 2 ^ {- t / \\text{half\\_life\\_time}}`
    :math:`\\rightarrow \\text{decay\\_param} = \\frac {\\ln(2)} {\\text{half\\_life\\_time}}`

    Any time unit [time] can be used for the input parameter.
    The output parameter will be defined in terms of the same time unit.

    Parameters
    ----------
    half_life_time : float
        Half-life time ([time]) of exponential distribution

    Returns
    -------
    float
        Decay parameter (1/[time]) of exponential distribution.

    """
    return np.log(2) / half_life_time


def half_life_from_decay_param(decay_param):
    """Convert half-life time to exponential decay parameter.

    :math:`e ^ {- \\text{decay\\_param} * t} = 2 ^ {- t / \\text{half\\_life\\_time}}`
    :math:`\\rightarrow \\text{half\\_life\\_time} = \\frac{\\ln(2)} {\\text{decay\\_param}}`

    Any time unit [time] can be used for the input parameter.
    The output parameter will be defined in terms of the same time unit.

    Parameters
    ----------
    decay_param : float
        Decay parameter (1/[time]) of exponential distribution.

    Returns
    -------
    float
        Half-life time ([time]) of exponential distribution

    """
    return np.log(2) / decay_param


def detection_in_time_window_probability(emission_time_decay_param, wave_function_decay_param, t_win=None):
    """Probability that a photon is detected within the detection time window.

    If a photon is emitted directly on a detector, and the detector only registers photons during a detection
    time window starting at the time when emission starts, how large is the probability that it registers a photon?
    (I.e. how large is the probability that the photon is detected within the detection time window?)

    Any time unit [time] can be used for input parameters, as long as it is used consistently between all of them.

    Parameters
    ----------
    emission_time_decay_param : float
        Decay parameter (1/[time]) of exponential distribution describing photon emission time.
    wave_function_decay_param : float
        Decay parameter (1/[time]) describing the exponential wave function of individual, pure photons.
    t_win : float or None, optional
        Time window used in the experiment. Photon detections outside the time window are not registered.
        If None, which is the default, no time window is used (i.e. infinite time window).

    """
    if t_win is None:
        return 1
    p = emission_time_decay_param * (1 - np.exp(- 2 * wave_function_decay_param * t_win))
    p -= 2 * wave_function_decay_param * (1 - np.exp(- emission_time_decay_param * t_win))
    p /= emission_time_decay_param - 2 * wave_function_decay_param
    return p


def detection_probability_density_function(time, emission_time_decay_param, wave_function_decay_param, start_time=0,
                                           t_win=None, renormalize=True):
    """Single-photon-detection probability density function.

    If a single photon is emitted directly onto a detector,
    what is the probability density for detecting it at each time?

    Any time unit [time] can be used for input parameters, as long as it is used consistently between all of them.

    Parameters
    ----------
    time : float
        Time ([time]) after emission for which the detection probability density is returned.
    emission_time_decay_param : float
        Decay parameter (1/[time]) of exponential distribution describing photon emission time.
    wave_function_decay_param : float
        Decay parameter (1/[time]) describing the exponential wave function of individual, pure photons.
    start_time : float, optional
        Time ([time]) at which emission starts. This is an offset on the entire distribution. Default zero.
        The probability density for detecting the photon at a `time < start_time` is zero.
    t_win : float or None, optional
        Time window ([time]) used in the experiment. Photon detections outside the time window are not registered.
        If None, which is the default, no time window is used (i.e. infinite time window).
        The probability density for detecting the photon at a time `time - start_time > t_win` is zero.
    renormalize : bool, optional
        If a finite detection time window is used, there is some probability that the photon is not detected at all.
        This results in a subnormalized probability density function.
        If this argument is true, which is the default,
        the function is multiplied with an overall factor that renormalizes it.
        Then, this function becomes the probability density function for the detection time on a photon,
        conditioned on the photon arriving within the detection time window.

    Returns
    -------
    float
        The probability density (1/[time]) for detecting the photon at time `time`.

    """
    time -= start_time  # make relative to start time

    if time < 0:  # photon emission has not yet begun
        return 0

    if t_win is not None and time > t_win:  # time lies outside of the detection time window
        return 0

    if emission_time_decay_param == 2 * wave_function_decay_param:
        # avoid division by zero; this is fine because the function is continuous at this point
        emission_time_decay_param += 1E-10
    p = np.exp(- 2 * wave_function_decay_param * time)
    p -= np.exp(- emission_time_decay_param * time)
    p *= 2 * wave_function_decay_param * emission_time_decay_param
    p /= emission_time_decay_param - 2 * wave_function_decay_param

    if t_win is not None and renormalize:
        p /= detection_in_time_window_probability(emission_time_decay_param=emission_time_decay_param,
                                                  wave_function_decay_param=wave_function_decay_param,
                                                  t_win=t_win)

    return p


def coincidence_probability(t_coin, emission_time_decay_param, wave_function_decay_param, t_win=None,
                            condition_on_within_time_window=True):
    """Coincidence probability of two non-interfering photons.

    Probability that two photons emitted simultaneously directly on two detectors are detected within
    the same coincidence time window, i.e. probability that there is not too much time between the two detections.

    Any time unit [time] can be used for input parameters, as long as it is used consistently between all of them.

    Parameters
    ----------
    t_coin : float
        Coincidence time window ([time]). This is the maximum time between the two photon detections.
        If the value is larger than the value of the detection time window `t_win`, that is effectively equivalent
        to not using a coincidence time window.
    emission_time_decay_param : float
        Decay parameter (1/[time]) of exponential distribution describing photon emission time.
    wave_function_decay_param : float
        Decay parameter (1/[time]) describing the exponential wave function of individual, pure photons.
    t_win : float or None, optional
        Time window ([time]) used in the experiment. Photon detections outside the time window are not registered.
        If None, which is the default, no time window is used (i.e. infinite time window).
    condition_on_within_time_window : bool, optional
        When using a finite detection time window, there is also a probability that no two photons are detected within
        it. In that case, they are not registered, and can thus also not lead to a coincidence.
        If this parameter is True, which it is by default, the returned coincidence probability is conditioned
        on both photons being detected within the detection time window.

    """

    if t_win is not None and t_coin > t_win:
        t_coin = t_win  # cap it
        # raise ValueError("Coincidence time window cannot exceed detection time window.")

    if emission_time_decay_param == 2 * wave_function_decay_param:
        # avoid division by zero, function should be continuous so this is fine
        emission_time_decay_param += 1E-10

    divisor_1 = emission_time_decay_param ** 2 - 4 * wave_function_decay_param ** 2

    term_1 = 1 - np.exp(- 2 * wave_function_decay_param * t_coin)
    term_1 *= emission_time_decay_param ** 2
    term_1 /= divisor_1

    term_2 = 1 - np.exp(- emission_time_decay_param * t_coin)
    term_2 *= - 4 * wave_function_decay_param ** 2
    term_2 /= divisor_1

    if t_win is None:
        return ((term_1 + term_2)
                / detection_in_time_window_probability(emission_time_decay_param=emission_time_decay_param,
                                                       wave_function_decay_param=wave_function_decay_param,
                                                       t_win=t_win) ** 2)

    divisor_2 = (emission_time_decay_param - 2 * wave_function_decay_param) ** 2

    term_3 = np.exp(- 4 * wave_function_decay_param * t_win)
    term_3 *= 1 - np.exp(2 * wave_function_decay_param * t_coin)
    term_3 *= emission_time_decay_param ** 2
    term_3 /= divisor_2

    term_4 = np.exp(- 2 * emission_time_decay_param * t_win)
    term_4 *= 1 - np.exp(emission_time_decay_param * t_coin)
    term_4 *= 4 * wave_function_decay_param ** 2
    term_4 /= divisor_2

    term_5 = np.exp(- (emission_time_decay_param + 2 * wave_function_decay_param) * t_win)
    term_5_component = emission_time_decay_param * np.exp(2 * wave_function_decay_param * t_coin)
    term_5_component += 2 * wave_function_decay_param * np.exp(emission_time_decay_param * t_coin)
    term_5_component /= emission_time_decay_param + 2 * wave_function_decay_param
    term_5 *= 1 - term_5_component
    term_5 *= - 4 * wave_function_decay_param * emission_time_decay_param
    term_5 /= divisor_2

    coin_prob = term_1 + term_2 + term_3 + term_4 + term_5

    if condition_on_within_time_window:
        coin_prob /= detection_in_time_window_probability(emission_time_decay_param=emission_time_decay_param,
                                                          wave_function_decay_param=wave_function_decay_param,
                                                          t_win=t_win) ** 2

    return coin_prob


def coincidence_probability_two_dark_counts(t_coin, t_win=None):
    """Probability that two dark counts that occur within the detection window are within one coincidence window.

    Any time unit [time] can be used for input parameters, as long as it is used consistently between all of them.

    Parameters
    ----------
    t_coin : float
        Coincidence time window ([time]). This is the maximum time between two detection events.
    t_win : float or None, optional
        Time window ([time]) used in the experiment. Photon detections outside the time window are not registered.
        If None, which is the default, no time window is used (i.e. infinite time window).

    Returns
    -------
    float
        Coincidence probability for two dark counts.

    """
    if t_win is None:
        return 0.
    if t_coin >= t_win:
        return 1.
    return 1 - np.square(1 - t_coin / t_win)


def coincidence_probability_dark_count_and_photon(t_coin, emission_time_decay_param, wave_function_decay_param,
                                                  t_win=None):
    """Probability that one photon and one dark count are detected within the same coincidence window.

    This probability is conditioned on both the photon detection and the dark count occurring within
    the detection time window.

    Any time unit [time] can be used for input parameters, as long as it is used consistently between all of them.

    Parameters
    ----------
    t_coin : float
        Coincidence time window ([time]). This is the maximum time between two detection events.
    emission_time_decay_param : float
        Decay parameter (1/[time]) of exponential distribution describing photon emission time.
    wave_function_decay_param : float
        Decay parameter (1/[time]) describing the exponential wave function of individual, pure photons.
    t_win : float or None, optional
        Time window ([time]) used in the experiment. Photon detections outside the time window are not registered.
        If None, which is the default, no time window is used (i.e. infinite time window).

    Returns
    -------
    float
        Coincidence probability for a dark count and a photon.

    """
    if t_win is None:
        return 0.
    if t_coin > t_win:
        return 1.

    factor = emission_time_decay_param * (1 - np.exp(- 2 * wave_function_decay_param * t_win))
    factor -= 2 * wave_function_decay_param * (1 - np.exp(- emission_time_decay_param * t_win))
    factor = 1 / factor
    factor /= t_win

    term_1 = 1 / (2 * wave_function_decay_param)
    term_1 -= t_coin
    term_1 -= np.exp(2 * wave_function_decay_param * t_coin) / (2 * wave_function_decay_param)
    term_1 *= np.exp(- 2 * wave_function_decay_param * t_win)
    term_1 += 1 / (2 * wave_function_decay_param)
    term_1 += t_coin
    term_1 -= np.exp(- 2 * wave_function_decay_param * t_coin) / (2 * wave_function_decay_param)
    term_1 *= emission_time_decay_param

    term_2 = 1 / emission_time_decay_param
    term_2 -= t_coin
    term_2 -= np.exp(emission_time_decay_param * t_coin) / emission_time_decay_param
    term_2 *= np.exp(- emission_time_decay_param * t_win)
    term_2 += 1 / emission_time_decay_param
    term_2 += t_coin
    term_2 -= np.exp(- emission_time_decay_param * t_coin) / emission_time_decay_param
    term_2 *= 2 * wave_function_decay_param

    return factor * (term_1 - term_2)


def visibility(t_coin, emission_time_decay_param, wave_function_decay_param, t_win=None):
    """Hong-Ou-Mandel visibility.

    Two identical but temporally impure photons are interfered on a 50/50 beamsplitter.
    What is their Hon-Ou-Mandel visibility, i.e. how large is the probability that both photons are detected
    in the same arm of the beamsplitter instead of in different ones?
    (V = 1 - R, where R = prob(same detector) / prob(different detector for non-interfering photons)).

    Any time unit [time] can be used for input parameters, as long as it is used consistently between all of them.

    Parameters
    ----------
    t_coin : float
        Coincidence time window ([time]). This is the maximum time between the two photon detections.
        If the value is larger than the value of the detection time window `t_win`, that is effectively equivalent
        to not using a coincidence time window.
    emission_time_decay_param : float
        Decay parameter (1/[time]) of exponential distribution describing photon emission time.
    wave_function_decay_param : float
        Decay parameter (1/[time]) describing the exponential wave function of individual, pure photons.
    t_win : float or None, optional
        Time window ([time]) used in the experiment. Photon detections outside the time window are not registered.
        If None, which is the default, no time window is used (i.e. infinite time window).

    Returns
    -------
    float
        Hong-Ou-Mandel visibility.

    """

    if t_win is not None and t_coin > t_win:
        t_coin = t_win  # increasing coincidence time window beyond time window has no effect

    # if np.isclose(t_coin, 0):
    if t_coin == 0.:
        return 1.

    term_1 = 1 - np.exp(- 2 * wave_function_decay_param * t_coin)
    term_1 *= emission_time_decay_param / (emission_time_decay_param + 2 * wave_function_decay_param)

    if t_win is None:
        return term_1 / coincidence_probability(t_coin=t_coin, emission_time_decay_param=emission_time_decay_param,
                                                wave_function_decay_param=wave_function_decay_param, t_win=t_win,
                                                condition_on_within_time_window=False)

    factor = emission_time_decay_param / (emission_time_decay_param - 2 * wave_function_decay_param) ** 2

    term_2 = np.exp(- 4 * wave_function_decay_param * t_win)
    term_2 *= 1 - np.exp(2 * wave_function_decay_param * t_coin)
    term_2 *= factor * emission_time_decay_param

    term_3 = np.exp(- 2 * emission_time_decay_param * t_win)
    term_3 *= 1 - np.exp(2 * (emission_time_decay_param - wave_function_decay_param) * t_coin)
    term_3 *= factor * 2 * wave_function_decay_param ** 2 / (emission_time_decay_param - wave_function_decay_param)

    term_4 = np.exp(- (emission_time_decay_param + 2 * wave_function_decay_param) * t_win)
    term_4 *= 1 - np.exp(emission_time_decay_param * t_coin)
    term_4 *= factor * 16 * wave_function_decay_param ** 2 / (emission_time_decay_param + 2 * wave_function_decay_param)
    term_4 *= -1

    vis = term_1 + term_2 + term_3 + term_4
    vis /= coincidence_probability(t_coin=t_coin, emission_time_decay_param=emission_time_decay_param,
                                   wave_function_decay_param=wave_function_decay_param, t_win=t_win,
                                   condition_on_within_time_window=False)

    return vis


def plot_detection_prob(emission_time_decay_param, wave_function_decay_param, t_win=None, t_max=None, renormalize=True):
    """Plot the single-photon-detection probability density function.

    Any time unit [time] can be used for input parameters, as long as it is used consistently between all of them.

    Parameters
    ----------

    emission_time_decay_param : float
        Decay parameter (1/[time]) of exponential distribution describing photon emission time.
    wave_function_decay_param : float
        Decay parameter (1/[time]) describing the exponential wave function of individual, pure photons.
    t_win : float or None, optional
        Time window ([time]) used in the experiment. Photon detections outside the time window are not registered.
        If None, which is the default, no time window is used (i.e. infinite time window).
        The probability density for detecting the photon at a time `time - start_time > t_win` is zero.
    t_max : float or None, optional
        Maximal time ([time]) to show in plot.
        If None, which is the default, `t_win` is used.
        If `t_win` is also None, an error is raised.
    renormalize : bool, optional
        If a finite detection time window is used, there is some probability that the photon is not detected at all.
        This results in a subnormalized probability density function.
        If this argument is true, which is the default,
        the function is multiplied with an overall factor that renormalizes it.
        Then, this function becomes the probability density function for the detection time on a photon,
        conditioned on the photon arriving within the detection time window.

    """
    if t_win is None and t_max is None:
        raise ValueError
    if t_max is None:
        t_max = t_win
    ts = np.linspace(1E-5, t_max)
    ps = [detection_probability_density_function(time=t,
                                                 emission_time_decay_param=emission_time_decay_param,
                                                 wave_function_decay_param=wave_function_decay_param,
                                                 t_win=t_win,
                                                 renormalize=renormalize)
          for t in ts]
    plt.plot(ts, ps)
    plt.title("Single-photon-detection probability-density function")
    plt.xlabel("Time")
    plt.ylabel("Probability density")


def plot_detection_in_time_window_probability(emission_time_decay_param, wave_function_decay_param, t_max=None):
    """Plot the probability that a single photon is detected within the detection time window vs window size.

    Any time unit [time] can be used for input parameters, as long as it is used consistently between all of them.

    Parameters
    ----------
    emission_time_decay_param : float
        Decay parameter (1/[time]) of exponential distribution describing photon emission time.
    wave_function_decay_param : float
        Decay parameter (1/[time]) describing the exponential wave function of individual, pure photons.
    t_max : float or None, optional
        Maximal detection time window ([time]) to show in plot.
        If None, which is the default,
        five times the half-life time of the photon emission probability distribution is used.

    """

    if t_max is None:
        t_max = half_life_from_decay_param(emission_time_decay_param) * 5
    ts = np.linspace(0, t_max)
    ps = [detection_in_time_window_probability(t_win=t,
                                               emission_time_decay_param=emission_time_decay_param,
                                               wave_function_decay_param=wave_function_decay_param,
                                               )
          for t in ts]
    plt.plot(ts, ps)
    plt.title("Probability single photon is detected within time window.")
    plt.xlabel("Time window")
    plt.ylabel("Probability")


def plot_coincidence_probability(emission_time_decay_param, wave_function_decay_param,
                                 t_win=None, t_max=None, condition_on_within_time_window=True):
    """Plot the coincidence probability as a function of the coincidence time window.

    Any time unit [time] can be used for input parameters, as long as it is used consistently between all of them.

    Parameters
    ----------
    emission_time_decay_param : float
        Decay parameter (1/[time]) of exponential distribution describing photon emission time.
    wave_function_decay_param : float
        Decay parameter (1/[time]) describing the exponential wave function of individual, pure photons.
    t_win : float or None, optional
        Time window ([time]) used in the experiment. Photon detections outside the time window are not registered.
        If None, which is the default, no time window is used (i.e. infinite time window).
    t_max : float or None, optional
        Maximal coincidence time window ([time]) to show in plot.
        If None, which is the default, `t_win` is used.
        If `t_win` is also None, an error is raised.
    condition_on_within_time_window : bool, optional
        When using a finite detection time window, there is also a probability that no two photons are detected within
        it. In that case, they are not registered, and can thus also not lead to a coincidence.
        If this parameter is True, which it is by default, the returned coincidence probability is conditioned
        on both photons being detected within the detection time window.

    """

    if t_win is None and t_max is None:
        raise ValueError
    if t_max is None:
        t_max = t_win
    t_coins = np.linspace(0, t_max)
    p_coins = [coincidence_probability(t_coin=t_coin,
                                       emission_time_decay_param=emission_time_decay_param,
                                       wave_function_decay_param=wave_function_decay_param,
                                       t_win=t_win,
                                       condition_on_within_time_window=condition_on_within_time_window)
               for t_coin in t_coins]
    plt.plot(t_coins, p_coins)
    plt.title("Coincidence probability for two photon detections")
    plt.xlabel("Coincidence time window")
    plt.ylabel("Coincidence probability")


def plot_coincidence_probs_all_types(emission_time_decay_param, wave_function_decay_param, t_win):
    """Plot coincidence probabilities vs coincidence time window for 2 photons, 2 dark counts, and photon + dark count.

    Any time unit [time] can be used for input parameters, as long as it is used consistently between all of them.

    Parameters
    ----------
    emission_time_decay_param : float
        Decay parameter (1/[time]) of exponential distribution describing photon emission time.
    wave_function_decay_param : float
        Decay parameter (1/[time]) describing the exponential wave function of individual, pure photons.
    t_win : float or None, optional
        Time window ([time]) used in the experiment. Photon detections outside the time window are not registered.
        If None, which is the default, no time window is used (i.e. infinite time window).

    """
    ts = np.linspace(0, t_win, 100)
    ps_ph_ph = [coincidence_probability(t_coin=t, emission_time_decay_param=emission_time_decay_param,
                                        wave_function_decay_param=wave_function_decay_param, t_win=t_win,
                                        condition_on_within_time_window=True)
                for t in ts]
    ps_ph_dc = [coincidence_probability_dark_count_and_photon(t_coin=t, t_win=t_win,
                                                              emission_time_decay_param=emission_time_decay_param,
                                                              wave_function_decay_param=wave_function_decay_param)
                for t in ts]
    ps_dc_dc = [coincidence_probability_two_dark_counts(t_coin=t, t_win=t_win) for t in ts]
    plt.plot(ts, ps_ph_ph, linestyle="-")
    plt.plot(ts, ps_ph_dc, linestyle="--")
    plt.plot(ts, ps_dc_dc, linestyle=":")
    plt.title("Coincidence probabilities")
    plt.xlabel("Coincidence time window")
    plt.ylabel("Coincidence probability")
    plt.legend(["photon - photon", "photon - dark count", "dark count - dark count"])


def plot_visibility(emission_time_decay_param, wave_function_decay_param, t_win=None, t_max=None):
    """Plot visibility versus coincidence time window.

    Any time unit [time] can be used for input parameters, as long as it is used consistently between all of them.

    Parameters
    ----------
    emission_time_decay_param : float
        Decay parameter (1/[time]) of exponential distribution describing photon emission time.
    wave_function_decay_param : float
        Decay parameter (1/[time]) describing the exponential wave function of individual, pure photons.
    t_win : float or None, optional
        Time window ([time]) used in the experiment (photon detections after this time are not registered).
        If None, no time window is used (i.e. infinite time window).
    t_max : float or None, optional
        Largest value of coincidence time window ([time]) to plot visibility for.
        If None (default), t_win is used.
        If t_win is also None, a ValueError is raised.

    """
    if t_win is None and t_max is None:
        raise ValueError
    if t_max is None:
        t_max = t_win
    ts = np.linspace(0, t_max, 100)
    ts = ts[1:]
    vs = [visibility(t_coin=t, emission_time_decay_param=emission_time_decay_param,
                     wave_function_decay_param=wave_function_decay_param, t_win=t_win)
          for t in ts]
    plt.plot(ts, vs)
    plt.title("Hong-Ou-Mandel Visibility")
    plt.xlabel("Coincidence time window")
    plt.ylabel("Visibility")


def compare_coincidence_prob_with_and_without_time_window(emission_time_decay_param, wave_function_decay_param, t_win):
    """Generate figure comparing coincidence probability with finite and infinite detection time window.

    Any time unit [time] can be used for input parameters, as long as it is used consistently between all of them.

    Parameters
    ----------
    emission_time_decay_param : float
        Decay parameter (1/[time]) of exponential distribution describing photon emission time.
    wave_function_decay_param : float
        Decay parameter (1/[time]) describing the exponential wave function of individual, pure photons.
    t_win : float
        Time window ([time]) in microseconds used in the experiment
        (photon detections after this time are not registered).

    """
    plot_coincidence_probability(emission_time_decay_param=emission_time_decay_param,
                                 wave_function_decay_param=wave_function_decay_param,
                                 t_win=None, t_max=t_win)
    plot_coincidence_probability(emission_time_decay_param=emission_time_decay_param,
                                 wave_function_decay_param=wave_function_decay_param,
                                 t_win=t_win, t_max=t_win)
    plt.legend(["no time window", "time window = {}".format(t_win)])


def compare_to_meraner_frequency_converted_data(t_win=None):
    """Compare simplified model to data from Meraner et al., for frequency-converted photons.

    The data is the data shown in Fig. 3 of
    "Indistinguishable photons from a trapped-ion quantum network node", Meraner et al.,
    https://arxiv.org/abs/1912.09259
    The data can be found [here](https://zenodo.org/record/4492161#.Yl7j6tpBxnL)
    (download and unzip `upload_data_HOM.zip` and look for the file
    `Analysis/figure_1550/plotter/wavepacket_from_Wavepackets64MHz_av_sn1_g0.txt`).

    Parameters
    ----------
    t_win : float, optional
        Time window (microseconds) used in the experiment (photon detections after this time are not registered).
        The data contains values for a coincidence time window up to 9 us.
        If None, no time window is used (i.e. infinite time window).
        It is unclear what value was used in the actual experiment,
        Using 9 seems reasonable, but the default value is None since this gives good agreement with data.

    Returns
    -------
    fig : matplotlib.figure.Figure
        Figure that contains the comparison.
    axs : list of matplotlib.axes.Axes
        Axes (subplots) of the figure.

    """
    # load data
    data_detection_prob = pd.read_csv("meraner_frequency_converted_detection_probability_short_path.csv")
    data_visibility_and_coincidence_prob = \
        pd.read_csv("meraner_frequency_converted_visibility_and_coincidence_prob_data.csv")

    # cut off after 9: that's where the laser is turned off
    data_detection_prob = data_detection_prob[data_detection_prob["x"] <= 9]
    data_detection_prob = data_detection_prob[data_detection_prob["x"] >= 0]
    data_visibility_and_coincidence_prob = \
        data_visibility_and_coincidence_prob[data_visibility_and_coincidence_prob["x"] <= 9]
    data_visibility_and_coincidence_prob = \
        data_visibility_and_coincidence_prob[data_visibility_and_coincidence_prob["x"] >= 0]
    data_coincidence_prob = pd.DataFrame()
    data_coincidence_prob["x"] = data_visibility_and_coincidence_prob["x"]
    data_coincidence_prob["y"] = data_visibility_and_coincidence_prob["coin_prob"]
    data_coincidence_prob["error"] = data_visibility_and_coincidence_prob["coin_prob_error"]
    data_visibility = pd.DataFrame()
    data_visibility["x"] = data_visibility_and_coincidence_prob["x"]
    data_visibility["y"] = data_visibility_and_coincidence_prob["vis"]
    data_visibility["error"] = data_visibility_and_coincidence_prob["vis_error"]

    return compare_to_data(data_detection_prob=data_detection_prob,
                           data_coincidence_prob=data_coincidence_prob,
                           data_visibility=data_visibility, t_win=t_win,
                           title="Comparison simplified model to frequency-converted data from Meraner et al.",
                           savepath="meraner_frequency_converted_data_comparison")


def compare_to_data(data_detection_prob, data_coincidence_prob, data_visibility, t_win, t_max=None,
                    title="Comparison to data", savepath=None):
    """Match and compare the simplified model for coincidence probability and visibility to a data set.

    There are two remaining free parameters in this model, namely the constant describing the exponential distribution
    for the photon emission time, and the constant describing the exponential wave packet of pure photons.
    These parameters are obtained by performing a least-squares fitting on all three data sets simultaneously:
    the detection probability, the coincidence probability and the visibility.
    Additionally, three other parameters are determined during the fitting:
    an offset ("start time") for the photon detection probability
    (otherwise, weird fitting results could be obtained due to an offset in the data),
    and normalization factors for the detection probability
    (if it is a histogram, this roughly converts it into a probability density function)
    and coincidence probability
    (data is typically not conditioned on the presence of photons, thus photon losses lower the detection
    and coincidence probabilities; if the loss probability is not known, it can be reverse engineered this way,
    allowing for a reasonable match between the model and the data).

    The resulting values for the parameters are then used to create a plot comparing the model predictions to the data.

    Parameters
    ----------
    data_detection_prob : pandas.DataFrame
        Data with photon detection time in microseconds as "x" column and detection probability as "y" column.
        Detection probability is automatically rescaled by this script to make it a probability density function,
        and to "remove" the effect of photon loss.
    data_coincidence_prob : pandas.DataFrame
        Data with coincidence time window in microseconds as "x" column and coincidence probability as "y" column.
        Coincidence probability is automatically rescaled by this script to "remove" the effect of photon loss.
    data_visibility : pandas.DataFrame
        Data with coincidence time window in microseconds as "x" column and visibility as "y" column.
    t_win : float
        Time window in microseconds used in the experiment (photon detections after this time are not registered).
    title : str, optional
        Title of the generated plot.
    savepath: str, optional
        Path to save the generated figure to. If None (default), it is not saved.

    Returns
    -------
    fig : matplotlib.figure.Figure
        Figure that contains the comparison.
    axs : list of matplotlib.axes.Axes
        Axes (subplots) of the figure.

    """

    if t_max is None:
        t_max = max(max(data_coincidence_prob["x"]), max(data_visibility["x"]), max(data_detection_prob["x"]))

    # fit parameters

    def errors(params):
        """Errors between model and data for specific parameter values.

        Compares to three data sets at once: detection probability, coincidence probability and visibility.
        All errors are combined into a single array that can be used by a least-squares fitting procedure.
        Different weights are given to the difference between the model and each specific data set.
        Small weight is given to the coincidence probability, since if the parameters produce a good detection
        probability, the coincidence probability should also automatically be good.
        The main reason it is included in here, is so a normalization factor can be determined that matches
        the model to the data.
        The visibility and detection probability need to have similar weights, since if one of the two is dominant,
        the parameters will be chosen to over fit one of them at the cost of having very bad fitting for the other.
        We have made the weight for the visibility a bit larger than for the detection probability,
        since in practice it is the more important function to match.
        Fitting results should still always be checked by a human to see if it is any good.

        Parameters
        ----------
        params : array-like
            First entry: decay parameter of the exponential distribution for the photon emission time.
            Second entry: decay parameter of the exponential wave packet of a pure photon.
            Third entry: start time (i.e. offset) for the detection probability.
            Fourth entry: normalization factor for the detection probability.
            Fifth entry: normalization factor for the coincidence probability.

        Returns
        -------
        list
            Errors between model and data for given parameters.
            Concatenation of the errors for the detection probability, coincidence probability, and visibility,
            each multiplied with some weight.

        """
        det_probs = [detection_probability_density_function(time=t, emission_time_decay_param=params[0],
                                                            wave_function_decay_param=params[1],
                                                            t_win=None, start_time=params[2], renormalize=True)
                     for t in data_detection_prob["x"]]
        # time window is ignored for detection probability
        det_probs_error = [(det_prob - det_prob_data / params[3]) * 1000000
                           for det_prob, det_prob_data in zip(det_probs, data_detection_prob["y"])]
        coin_probs = [coincidence_probability(t_coin=t, emission_time_decay_param=params[0],
                                              wave_function_decay_param=params[1], t_win=t_win)
                      for t in data_coincidence_prob["x"]
                      ]
        coin_probs_error = [(coin_prob - coin_prob_data / params[4])
                            for coin_prob, coin_prob_data in zip(coin_probs, data_coincidence_prob["y"])]
        viss = [visibility(t_coin=t, emission_time_decay_param=params[0], wave_function_decay_param=params[1],
                           t_win=t_win)
                for t in data_visibility["x"]]
        vis_errors = [(vis - vis_data) * 100000
                      for vis, vis_data in zip(viss, data_visibility["y"])]
        err = det_probs_error + coin_probs_error + vis_errors
        return err
    emission_time_decay_param, wave_function_decay_param, start_time, norm_fact_det_prob, norm_fact_coin_prob = \
        least_squares(errors, [decay_param_from_half_life(3), decay_param_from_half_life(2), 0.5, 0.1, 0.1],
                      bounds=([decay_param_from_half_life(40), decay_param_from_half_life(8), 0, 1E-5, 1E-5],
                              [decay_param_from_half_life(1), decay_param_from_half_life(0.1), 10, 1, 1])).x

    print(f"Fitted half lifes. "
          f"Emission probability distribution: {half_life_from_decay_param(emission_time_decay_param)}. "
          f"Wave function: {half_life_from_decay_param(wave_function_decay_param)}.")

    # calculate values predicted by the simplified model
    ts = np.linspace(1E-5, t_max, 100)
    detection_probs = [detection_probability_density_function(time=t,
                                                              emission_time_decay_param=emission_time_decay_param,
                                                              wave_function_decay_param=wave_function_decay_param,
                                                              start_time=start_time, t_win=None, renormalize=True)
                       for t in ts]
    coin_probs = [coincidence_probability(t_coin=t, emission_time_decay_param=emission_time_decay_param,
                                          wave_function_decay_param=wave_function_decay_param, t_win=t_win) for t in ts]
    vis = [visibility(t_coin=t, emission_time_decay_param=emission_time_decay_param,
                      wave_function_decay_param=wave_function_decay_param, t_win=t_win) for t in ts]

    # make plots
    fig, axs = plt.subplots(3, sharex=True)
    # fig.suptitle(title)
    legend = ["Meraner et al.", "Model"]
    if "error" in data_detection_prob.columns:
        axs[0].errorbar(x=data_detection_prob["x"], y=data_detection_prob["y"] / norm_fact_det_prob,  # normalized
                        yerr=data_detection_prob["error"] / norm_fact_det_prob, marker="o", markersize=2)
    else:
        axs[0].plot(data_detection_prob["x"], data_detection_prob["y"] / norm_fact_det_prob,  # normalized
                    marker="o", markersize=2)
    axs[0].plot(ts, detection_probs, alpha=0.7)
    axs[0].set_ylabel("Detection\nprobability\ndensity")
    axs[0].set_xlabel(r"Time ($\mu$s)")
    if "error" in data_coincidence_prob.columns:
        axs[1].errorbar(x=data_coincidence_prob["x"], y=data_coincidence_prob["y"] / norm_fact_coin_prob,  # normalized
                        yerr=data_coincidence_prob["error"] / norm_fact_coin_prob, marker="o", markersize=2)
    else:
        axs[1].plot(data_coincidence_prob["x"], data_coincidence_prob["y"] / norm_fact_coin_prob,
                    marker="o", markersize=2)
    axs[1].plot(ts, coin_probs, alpha=0.7)
    axs[1].set_ylabel("Coincidence\nprobability")
    axs[1].set_yticks([0, 0.5, 1])
    if "error" in data_coincidence_prob.columns:
        axs[2].errorbar(x=data_visibility["x"], y=data_visibility["y"],
                        yerr=data_visibility["error"], marker="o", markersize=2)
    else:
        axs[2].plot(data_visibility["x"], data_visibility["y"], marker="o", markersize=2)
    axs[2].plot(ts, vis, alpha=0.7)
    axs[2].set_xlabel(r"Coincidence time window ($\mu$s)")
    axs[2].set_ylabel("Visibility")
    axs[2].set_xticks([0, 3, 6, 9])
    fig.tight_layout()
    plt.legend(legend)

    if savepath is not None:
        plt.savefig(savepath, bbox_inches="tight", dpi=300)

    return fig, axs


# Parameter used for Innsbruck experiment
innsbruck_t_win = 17.5
# Parameters obtained from fitting to Innsbruck data
innsbruck_emission_time_decay_param = decay_param_from_half_life(6.79)
innsbruck_wave_function_decay_param = decay_param_from_half_life(3.01)


if __name__ == "__main__":

    # Below commented functions generate plots of the model from fitting to the Innsbruck data.
    # This is the fit that is used in the Delft - Eindhoven paper.
    # The data itself is not shown as it has not been published yet.

    # plot_detection_prob(emission_time_decay_param=innsbruck_emission_time_decay_param,
    #                     wave_function_decay_param=innsbruck_wave_function_decay_param,
    #                     t_win=innsbruck_t_win, t_max=None, renormalize=False)

    # plot_detection_in_time_window_probability(emission_time_decay_param=innsbruck_emission_time_decay_param,
    #                                           wave_function_decay_param=innsbruck_wave_function_decay_param)

    # plot_coincidence_probability(emission_time_decay_param=innsbruck_emission_time_decay_param,
    #                              wave_function_decay_param=innsbruck_wave_function_decay_param,
    #                              t_win=innsbruck_t_win, t_max=None, condition_on_within_time_window=True)

    # plot_coincidence_probs_all_types(emission_time_decay_param=innsbruck_emission_time_decay_param,
    #                                  wave_function_decay_param=innsbruck_wave_function_decay_param,
    #                                  t_win=innsbruck_t_win)

    # plot_visibility(emission_time_decay_param=innsbruck_emission_time_decay_param,
    #                 wave_function_decay_param=innsbruck_wave_function_decay_param,
    #                 t_win=innsbruck_t_win, t_max=None)

    # Fit to data from Meraner paper (to verify that this toy model is able to capture the tradeoff).
    compare_to_meraner_frequency_converted_data(None)

    # plt.show()
