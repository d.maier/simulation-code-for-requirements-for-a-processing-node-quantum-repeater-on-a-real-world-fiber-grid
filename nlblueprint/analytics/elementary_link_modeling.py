import numpy as np
from sympy import shape, symbols, conjugate, sqrt, simplify, gcd
from sympy.matrices import Matrix, zeros, eye, diag
from sympy.physics.quantum.tensorproduct import TensorProduct


# symbol for visibility
mu = symbols("mu")  # visibility

# symbols for loss probabilities
prob_loss_a, prob_loss_b = symbols("p_A, p_B", positive=True)


# POVM elements of a single Hong-Ou-Mandel detection scheme (i.e. beam splitter + 2 single-photon detectors)
# The POVMs are the effective POVMs for two photons that are not perfectly indistinguishable
# state |0> indicates no photon, |1> indicates presence of a specific wave package.
# The POVMs are obtained from the link-layer paper:
# "A Link Layer Protocol For Quantum Networks, A. Dahlberg et al., 10.1145/3341302.3342070
# These POVMs correspond to the case when there are no dark counts.
# Note: only POVMs that can contribute to a heralded success have been included.

# M00 POVM element
m00 = diag(1, 0, 0, 0)

# M10 POVM element
inner_part_m10 = eye(2)
inner_part_m10[0, 1] = mu
inner_part_m10[1, 0] = conjugate(mu)
m10 = diag(0, inner_part_m10, 0) / 2

# M01 POVM element
inner_part_m01 = eye(2)
inner_part_m01[0, 1] = - mu
inner_part_m01[1, 0] = - conjugate(mu)
m01 = diag(0, inner_part_m01, 0) / 2

# M20 POVM element
m20 = zeros(4)
m20[3, 3] = (1 + abs(mu) ** 2) / 4

# M02 POVM element
m02 = zeros(4)
m02[3, 3] = (1 + abs(mu) ** 2) / 4


# Now, we define the effective POVM elements in the presence of dark counts.
# Dark counts are modelled as a perfect detection using the POVM defined above,
# but giving the wrong classical detection outcome sometimes.
# This gives rise to the following effective dark counts.

prob_dc = symbols("p_dc", positive=True)

# Effective M00 POVM element in presence of dark counts.
# Only zero clicks if there are zero photons and zero dark counts.
m00_dc = m00 * (1 - prob_dc) ** 2

# Effective M01 POVM element in presence of dark counts.
# Only if there was one photon in the second bin and no dark counts,
# or no photons and one dark count in the correct detector
m01_dc = m01 * (1 - prob_dc) ** 2 + m00 * (1 - prob_dc) * prob_dc

# Effective M10 POVM element is derived using the same logic as the M01 POVM elmeent
m10_dc = m10 * (1 - prob_dc) ** 2 + m00 * (1 - prob_dc) * prob_dc

# Effective M02 POVM element in prsence of dark counts.
# We cheat a little and include any multi-photon detection in the second detector in the M02 POVM element.
# Thus, if there is a dark count in the second bin which leads to the detection of three photons,
# this is not distinguished from the case where there is no dark count and there are two photons detected.
# The reason for this is that all multi-photon detections will be handled in the same manner by the
# entanglement-generation protocol.
# There can never be a dark-count in the first detector, so there is an overall factor (1 - dark-count probability).
# Note: we model dark counts as occuring at most once per detector.
# Thus, when there are no photons incoming, it is impossible for a detector to give a photon count of two.
# TODO is this accurate or should there be a M00 * pdc ** 2 component?
m02_dc = (1 - prob_dc) * (m02 + m01 * prob_dc)

# Effective M20 POVM element in presence of dark counts is derived in the same way as the m02 one.
m20_dc = (1 - prob_dc) * (m20 + m10 * prob_dc)

# SWAP matrix (operation that interchanges two qubits)
swap = diag(1, Matrix([[0, 1], [1, 0]]), 1)


def post_measurement_state(povm_element, input_state):
    """Calculate the post-measurement state corresponding to a specific POVM element, after tracing out
    all but first two qubits.

    POVM elements in principle do not contain information about the post-measurement state;
    for that, you would need the actual measurement (Kraus) operators describing the measurement.
    However, if the part of the system that is measured is traced out immediately afterwards,
    the POVM elements do provide enough information.

    This function applies the POVM to the state, and then traces out all dimensions except for the first 4.

    Parameters
    ----------
    povm_element : :class:`sympy.Matrix`
        Element of POVM.
    input_state : :class:`sympy.Matrix`
        Density matrix of pre-measurement state.

    Returns
    -------
    :class:`sympy.Matrix`
        Post-measurement state (density matrix) if the outcome corresponding to the POVM element is obtained.

    """
    state_without_partial_trace = povm_element * input_state
    state_with_partial_trace = partial_trace_last_qubits(state_without_partial_trace)
    return simplify(state_with_partial_trace)


def partial_trace_last_qubits(state):
    """Trace out last x qubits of density matrix, where x is chosen such that a two-qubit density matrix remains.

    Parameters
    -----------
    state : :class:`sympy.Matrix`
        Density matrix of 2 or more qubits.

    Returns
    -------
    :class:`sympy.Matrix`
        4 x 4 matrix corresponding to reduced density matrix of remaining two qubits.

    """
    state_dimension = shape(state)[0]
    dimensions_to_trace_over = int(state_dimension / 4)  # 4 is dimension of 2 qubits
    state_after_partial_trace = zeros(4)
    for i in range(dimensions_to_trace_over):
        basis_vector = Matrix([0] * dimensions_to_trace_over)
        basis_vector[i] = 1
        trace_operator = TensorProduct(eye(4), basis_vector)
        term_of_partial_trace = trace_operator.H * state * trace_operator
        state_after_partial_trace = state_after_partial_trace + term_of_partial_trace
    return state_after_partial_trace


def amplitude_dampen(state, damp_qubit, loss_probability):
    """Perform amplitude damping on a qubit.

    Parameters
    ----------
    state : :class:`sympy.Matrix`
        Density matrix of state to perform amplitude damping on.
    damp_qubit : int
        State lives in Hilbert space H1 x H2 x ... Hn, where Hi is the Hilbert space of the ith qubit.
        damp_qubit is the i corresponding to the qubit that is amplitude dampened.
    loss_probability : :class:`sympy.Symbol`
        Symbol representing the probability that state of photon number 1 is dampened to photon number 0.
        Assumed to be real and positive.

    Notes
    -----
    Implemented using Kraus operators (see e.g. https://en.wikipedia.org/wiki/Amplitude_damping_channel ).
    However, instead of using output_state = sum_i K_i input_state hermitian_conjugate(K_i),
    we are using output_state = sum_i K_i input_state transpose(K_i).
    This is valid if loss_probability is real and positive, and makes it easier for sympy to simplify expressions.

    """
    k0 = diag(1, sqrt(1 - loss_probability))
    k1 = zeros(2)
    k1[0, 1] = sqrt(loss_probability)

    num_qubits_before_damp_qubit = int(damp_qubit - 1)
    state_dimension = shape(state)[0]
    num_qubits = int(np.log2(state_dimension))
    num_qubits_after_damp_qubit = int(num_qubits - damp_qubit)

    k0_embedded = TensorProduct(eye(2 ** num_qubits_before_damp_qubit), k0, eye(2 ** num_qubits_after_damp_qubit))
    k1_embedded = TensorProduct(eye(2 ** num_qubits_before_damp_qubit), k1, eye(2 ** num_qubits_after_damp_qubit))

    return k0_embedded * state * k0_embedded.T + k1_embedded * state * k1_embedded.T


def prettify_unnormalized_state(state):
    """Find common divider of matrix elements and divide it out. Then simplify.

    If a state is not normalized anyway, it is possible to make it look nicer by dividing out common factors from
    the matrix elements.
    That is exactly what this function does.

    Parameters
    ----------
    state : :class:`sympy.Matrix`
        Unnormalized density matrix.

    """
    return simplify(state / gcd([i for i in state]))
