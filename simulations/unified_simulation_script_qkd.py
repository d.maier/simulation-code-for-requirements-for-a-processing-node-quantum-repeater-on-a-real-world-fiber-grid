import numpy as np
from argparse import ArgumentParser
import yaml
import time
import pandas as pd
from qlink_interface import MeasurementBasis, ReqMeasureDirectly, ResMeasureDirectly
from nlblueprint.egp_datacollector import EGPDataCollector
from simulations.unified_simulation_script_state import setup_networks, find_varied_param, save_data,\
    magic_and_protocols, run_simulation
import netsquid as ns
from netsquid_simulationtools.repchain_dataframe_holder import RepchainDataFrameHolder
from netsquid_simulationtools.repchain_data_process import process_repchain_dataframe_holder, process_data_duration, \
    process_data_bb84
from netsquid_simulationtools.repchain_data_plot import plot_qkd_data
from netsquid_netconf.netconf import Loader

distance_delft_eindhoven = 226.5

"""
This script takes as input a configuration file and a parameter file. You can find examples of these for each of the
platforms we simulate in the same folder. It parses these files using netconf and runs a BB84 experiment using a
link layer protocol. It then stores this data in a RepChainDataFrameHolder object and saves it in a pickle file.
The script is capable of detecting if a parameter is being varied in a configuration file and, using the netconf
snippet, run the simulation for each of the values of this parameter.

Through optional input arguments one can define the path where the results should be saved, how many runs per data
point to simulate, the name of the saved file and whether the simulation results should be plotted.
"""


def collect_qkd_data(generator, n_runs, sim_params, varied_param,
                     varied_object, number_nodes, suppress_output=False):
    """
    Runs simulation and collects data for each network configuration. Stores data in RepchainDataFrameHolder in format
    that allows for plotting at later point.

    Parameters
    ----------
    generator : generator
        Generator of network configurations
    n_runs : int
        Number of runs per data point
    sim_params : dict or None
        Dictionary holding simulation parameters
    varied_param : str or None
        Name of parameter being varied
    varied_object : str or None
        Name of object whose parameter is being varied
    number_nodes : int
        Number of nodes in chain being simulated
    suppress_output : bool
        If true, status print statements are suppressed.

    Returns
    -------
    meas_holder : :class:`netsquid_simulationtools.repchain_dataframe_holder.RepchainDataFrameHolder`
        RepchainDataFrameHolder with collected simulation data.

    """
    ns.set_qstate_formalism(ns.qubits.qformalism.QFormalism.KET)

    if not suppress_output:
        if varied_param is None:
            print("Simulating", n_runs, "runs")
        else:
            print("Simulating", n_runs, "runs for each value of", varied_param)

    data = []
    measurement_bases = [MeasurementBasis.X, MeasurementBasis.Z]
    for objects, config in generator:
        data_one_configuration = []
        network = objects["network"]
        egp_services = magic_and_protocols(network, config, sim_params)
        start_time_simulation = time.time()
        for basis in measurement_bases:
            if basis == MeasurementBasis.X:
                exp_spec_args = {"y_rotation_angle_local": np.pi / 2, "y_rotation_angle_remote": np.pi / 2}
            else:
                exp_spec_args = {"y_rotation_angle_local": 0, "y_rotation_angle_remote": 0}

            data_one_meas_basis = run_simulation(egp_services=egp_services,
                                                 request_type=ReqMeasureDirectly,
                                                 response_type=ResMeasureDirectly,
                                                 data_collector=EGPDataCollector,
                                                 n_runs=n_runs,
                                                 experiment_specific_arguments=exp_spec_args)
            if varied_param is not None:
                param_value = config["components"][varied_object]["properties"][varied_param]
                data_one_meas_basis[varied_param] = param_value
            data_one_configuration.append(data_one_meas_basis)
        simulation_time = time.time() - start_time_simulation
        if not suppress_output:
            if varied_param is None:
                print(f"Performed {n_runs} runs in X and Z {simulation_time:.2e} s")
            else:
                print(
                    f"Performed {n_runs} runs in X and Z for {varied_param} = {param_value} in {simulation_time:.2e} s")
        data.append(data_one_configuration)

    flat_data = [item for sublist in data for item in sublist]
    data = pd.concat(flat_data, ignore_index=True)
    new_data = data.drop(labels=["time_stamp", "entity_name"], axis="columns")

    if sim_params is not None:
        baseline_parameters = sim_params
        baseline_parameters["number_nodes"] = number_nodes
    else:
        baseline_parameters = {"length": distance_delft_eindhoven,
                               "number_nodes": number_nodes}

    meas_holder = RepchainDataFrameHolder(baseline_parameters=baseline_parameters, data=new_data, number_of_nodes=2)

    return meas_holder


def run_unified_simulation_qkd(configfile, paramfile=None, n_runs=10, suppress_output=False):
    """Run unified simulation script.

    Parameters
    ----------
    config_file_name : str
        Name of configuration file.
    param_file_name : str
        Name of parameter file.
    n_runs : int
        Number of runs per data point
    suppress_output : bool
        If true, status print statements are suppressed.

    Returns
    -------
    meas_holder : :class:`netsquid_simulationtools.repchain_dataframe_holder.RepchainDataFrameHolder`
        RepchainDataFrameHolder with collected QKD data.
    varied_param : str
        Name of parameter that is being varied in the simulation.

    """

    generator, number_nodes = setup_networks(configfile, paramfile)
    varied_object, varied_param = find_varied_param(generator)
    generator, _ = setup_networks(configfile, paramfile)

    # iterate over networks read from config file and collect BB84 data
    if paramfile is not None:
        with open(paramfile, "r") as stream:
            sim_params = yaml.load(stream, Loader=Loader)
    else:
        sim_params = None

    repchain_df_holder = collect_qkd_data(generator, n_runs, sim_params, varied_param, varied_object,
                                          number_nodes, suppress_output=suppress_output)

    return repchain_df_holder, varied_param


def plot_data(meas_holder, skr_minmax=True):
    """
    Plots QKD data using the simulation tools snippet.

    Parameters
    ----------
    meas_holder : :class:`netsquid_simulationtools.repchain_dataframe_holder.RepchainDataFrameHolder`
        RepchainDataFrameHolder with collected QKD data.

    """
    if len(meas_holder.varied_parameters) != 1:
        raise ValueError("Can only plot for data with exactly one varied parameter. "
                         f"This data has the following varied parameters: {meas_holder.varied_parameters}")
    [varied_param] = meas_holder.varied_parameters
    processed_data = process_repchain_dataframe_holder(repchain_dataframe_holder=meas_holder,
                                                       processing_functions=[process_data_duration,
                                                                             process_data_bb84])
    processed_data.to_csv("output.csv", index=False)
    plot_qkd_data(filename="output.csv", scan_param_name=varied_param,
                  scan_param_label=varied_param, shaded=False, skr_minmax=skr_minmax)


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument('configfile', type=str, help="Name of the config file.")
    parser.add_argument('-pf', '--paramfile', required=False, type=str,
                        help="Name of the parameter file.")
    parser.add_argument('-n', '--n_runs', required=False, type=int, default=10,
                        help="Number of runs per configuration. If none is provided, defaults to 10.")
    parser.add_argument('--output_path', required=False, type=str, default="raw_data",
                        help="Path relative to local directory where simulation results should be saved.")
    parser.add_argument('--filebasename', required=False, type=str, help="Name of the file to store results in.")
    parser.add_argument('--plot', dest="plot", action="store_true", help="Plot the simulation results.")

    args, unknown = parser.parse_known_args()

    repchain_df_holder, varied_param = run_unified_simulation_qkd(configfile=args.configfile,
                                                                  paramfile=args.paramfile,
                                                                  n_runs=args.n_runs,
                                                                  suppress_output=False)
    save_data(repchain_df_holder, args)
    if args.plot:
        plot_data(repchain_df_holder)
