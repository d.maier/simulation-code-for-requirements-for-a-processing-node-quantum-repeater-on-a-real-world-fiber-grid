#!/bin/bash
#SBATCH --nodes=1
#SBATCH --time=9:00:00
#SBATCH --partition=thin

identifier=$1
source ~/blueprint_env/bin/activate
./run_local.sh input_file.ini $identifier
