#!/bin/bash

number_runs=$1
for ((n=0;n<number_runs;n++)); do
	sbatch --exclusive ./run_local_bg.sh $n
	sleep 1
done
