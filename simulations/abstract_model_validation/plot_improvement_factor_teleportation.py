import numpy as np
import matplotlib.pyplot as plt
import pandas
from argparse import ArgumentParser
from netsquid_simulationtools.repchain_data_plot import _set_font_sizes


def plot_two_platforms_teleportation_fidelity(file_name_1, file_name_2, label_1, label_2):
    """Taking processed data, plots average teleportation fidelity and entanglement generation rate against hardware
    improvement factor for two platforms.

    Parameters
    ----------
    file_name_1 : str
        Name of file containing processed data for platform 1.
    file_name_2 : str
        Name of file containing processed data for platform 2.
    label_1 : str
        Label to display for platform 1.
    label_2 : str
        Label to display for platform 2.
    """
    scan_param_label = "improvement_factor"
    df_1 = pandas.read_csv(file_name_1)
    df_2 = pandas.read_csv(file_name_2)

    df_1 = df_1.sort_values(by=[scan_param_label])
    df_2 = df_2.sort_values(by=[scan_param_label])

    num_plots = 2
    _set_font_sizes(medium=14)
    fig, ax = plt.subplots(1, num_plots, figsize=(6 * num_plots, 5))

    fidelity_ax = ax if num_plots == 1 else ax[0]
    fidelity_ax.set_xlabel(scan_param_label)
    fidelity_ax.set_ylabel("Teleportation Fidelity")

    df_1.plot(x=scan_param_label, y="teleportation_fidelity_average",
              yerr="teleportation_fidelity_average_error",
              capsize=4, kind="line", color="red", label="Average " + label_1,
              ax=fidelity_ax)
    # df_1.plot(x=scan_param_label, y="teleportation_fidelity_average_optimized_local_unitaries", kind="line",
    #           style="r:", linewidth=3,
    #           label="Average optimized " + label_1, ax=fidelity_ax)
    df_2.plot(x=scan_param_label, y="teleportation_fidelity_average",
              yerr="teleportation_fidelity_average_error",
              capsize=4, kind="line", color="green", label="Average " + label_2,
              ax=fidelity_ax)
    # df_2.plot(x=scan_param_label, y="teleportation_fidelity_average_optimized_local_unitaries", kind="line",
    #           style="g:", linewidth=3, label="Average optimized " + label_2, ax=fidelity_ax)

    df_1.plot(x=scan_param_label, y="duration_per_success", kind="scatter", color="red",
              yerr="duration_per_success_error", ax=ax[1], legend=True, logy=True, grid=True,
              label=label_1)
    df_2.plot(x=scan_param_label, y="duration_per_success", kind="scatter", color="green",
              yerr="duration_per_success_error", ax=ax[1], legend=True, logy=True, grid=True,
              label=label_2)

    fit = np.poly1d(np.polyfit(df_1.get(scan_param_label), np.log(df_1.duration_per_success), deg=5))
    att_fit = []
    for k in range(len(df_1.get(scan_param_label))):
        att_fit.append(np.exp(fit(df_1.get(scan_param_label)[k])))
    ax[1].plot(df_1.get(scan_param_label), att_fit, 'r')

    if df_1.generation_duration_unit[0] == "seconds":
        ax[1].set_ylabel("Avg. num. of s/succ.")
    elif df_1.generation_duration_unit[0] == "rounds":
        ax[1].set_ylabel("Avg. num. of att./succ.")
    else:
        ax[1].set_ylabel(f"Avg. num. of {df_1.generation_duration_unit[0]}/succ.")

    fit = np.poly1d(np.polyfit(df_2.get(scan_param_label), np.log(df_2.duration_per_success), deg=5))
    att_fit = []
    for k in range(len(df_2.get(scan_param_label))):
        att_fit.append(np.exp(fit(df_2.get(scan_param_label)[k])))
    ax[1].plot(df_2.get(scan_param_label), att_fit, 'g')

    if df_2.generation_duration_unit[0] == "seconds":
        ax[1].set_ylabel("Avg. num. of s/succ.")
    elif df_2.generation_duration_unit[0] == "rounds":
        ax[1].set_ylabel("Avg. num. of att./succ.")
    else:
        ax[1].set_ylabel(f"Avg. num. of {df_2.generation_duration_unit[0]}/succ.")

    ax[1].set_xlabel(scan_param_label)
    plt.legend()
    plt.show()


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("--file_1", required=True, type=str, help="CSV file with teleportation data of platform 1.")
    parser.add_argument("--file_2", required=True, type=str, help="CSV file with teleportation data of platform 1.")
    parser.add_argument("--label_1", required=False, type=str, default="Platform 1")
    parser.add_argument("--label_2", required=False, type=str, default="Platform 2.")

    args = parser.parse_args()

    plot_two_platforms_teleportation_fidelity(file_name_1=args.file_1, file_name_2=args.file_2,
                                              label_1=args.label_1, label_2=args.label_2)
