from ruamel.yaml import YAML
import sys
import numpy as np
import netsquid as ns
import yaml
import csv
from argparse import ArgumentParser
from simulations.unified_simulation_script_state import setup_networks, collect_state_data
from netsquid_simulationtools.repchain_data_functions import estimate_duration_per_success
from netsquid_simulationtools.process_teleportation import estimate_average_teleportation_fidelity_from_data,\
    determine_teleportation_fidelities_of_xyz_eigenstates_from_data
from netsquid_simulationtools.parameter_set import rootbased_improvement_fn
from netsquid_abstractmodel.abstract_parameter_set import AbstractParameterSet
from netsquid_nv.nv_parameter_set import NVParameterSet
from nlblueprint.ion_trap.ti_parameter_set import TIParameterSet
from netsquid_netconf.netconf import Loader

PARAMETER_SETS = {
    "abstract": AbstractParameterSet,
    "nv": NVParameterSet,
    "ti": TIParameterSet
}

PLATFORM_COHERENCE = {
    "abstract": "T2",
    "nv": "carbon_T2",
    "ti": "coherence_time"
}


def dump_to_file(file_name, params):
    """Write a dictionary to a yaml file.

    Parameters
    ----------
    file_name : str
        Name of file to be written.
    params : dict
        Dictionary of parameters.
    """

    with open(file_name, "w") as stream:
        yaml.dump(params, stream)


def update_config_file(config_file, param_file, platform):
    """Update network configuration to have it include a given parameter file.

    Note that this assumes that the anchor for importing the simulation parameters from the parameter file has the
    structure `platform_sim_params`.

    Parameters
    ----------
    config_file : str
        Name of configuration file.
    param_file : str
        Name of parameter file.
    platform : str
        Name of platform.
    """

    params_str_identifier = str(platform) + '_sim_params'

    yaml = YAML()
    with open(config_file, 'r') as stream:
        code = yaml.load(stream)
    code[params_str_identifier]["INCLUDE"].value = param_file
    with open(config_file, "w") as stream:
        yaml.dump(code, stream)


def read_from_file(file_name):
    """Read parameters from yaml file.

    Parameters
    ----------
    file_name : str
        Name of file to be read.

    Returns
    -------
    sim_params : dict
        Dictionary holding parameters read from file.
    """

    with open(file_name, "r") as stream:
        sim_params = yaml.load(stream, Loader=Loader)

    return sim_params


def setup_simulation(config_file, param_file):
    """Set up the simulation according to the given configuration and parameter files.

    Parameters
    ----------
    config_file_name : str
        Name of configuration file.
    param_file_name : str
        Name of parameter file.

    Returns
    -------
    generator : generator
        Generator yielding network configurations.
    ae : bool
        True if simulating atomic ensemble hardware, False otherwise.
    number_nodes : int
        Number of nodes in the network.
    sim_params : dict
        Dictionary with simulation parameters.
    """

    ns.sim_reset()
    generator, ae, number_nodes = setup_networks(config_file_name=config_file, param_file_name=param_file)
    with open(param_file, "r") as stream:
        sim_params = yaml.load(stream, Loader=Loader)

    return generator, ae, number_nodes, sim_params


def improve_parameter_file(param_file, improvement_factor, param_to_improve, hardware_platform, baseline_sim_params):
    """Take parameter file, improve parameter by improvement factor according to root based improvement and dump result
    to new parameter file.

    Parameters
    ----------
    param_file : str
        Name of parameter file.
    improvement_factor : float
        Factor by which to improve parameter.
    param_to_improve : str
        Name of parameter to be improved.
    hardware_platform : str
        Name of hardware platform. Possibilities are `abstract`, `nv` and `ti`.
    baseline_sim_params : dict
        Dictionary with baseline simulation parameters.

    Returns
    -------
    param_file : str
        Name of parameter improved parameter file.
    """

    try:
        ParameterSetClass = PARAMETER_SETS[hardware_platform]
    except KeyError:
        print(hardware_platform, "not recognized. Currently supported hardware platforms are", list(PARAMETER_SETS))
        sys.exit(1)

    if improvement_factor > 0:
        # ugly workaround needed due to parameter naming inconsistencies
        if param_to_improve == "tau_decay":
            improvement_dictionary = {"product_tau_decay_delta_w": improvement_factor}
            baseline_sim_params["product_tau_decay_delta_w"] = baseline_sim_params[param_to_improve]
        elif param_to_improve == "detector_efficiency":
            improvement_dictionary = {"prob_detect_excl_transmission_no_conversion_no_cavities": improvement_factor}
            baseline_sim_params["prob_detect_excl_transmission_no_conversion_no_cavities"] = \
                baseline_sim_params[param_to_improve]
        else:
            improvement_dictionary = {param_to_improve: improvement_factor}
        improved_parameter_dict = ParameterSetClass.to_improved_dict(baseline_sim_params,
                                                                     improvement_dictionary,
                                                                     rootbased_improvement_fn)
        if param_to_improve == "tau_decay":
            improved_parameter_dict[param_to_improve] = improved_parameter_dict["product_tau_decay_delta_w"]
            del improved_parameter_dict["product_tau_decay_delta_w"]
        elif param_to_improve == "detector_efficiency":
            improved_parameter_dict[param_to_improve] = \
                improved_parameter_dict["prob_detect_excl_transmission_no_conversion_no_cavities"]
            del improved_parameter_dict["prob_detect_excl_transmission_no_conversion_no_cavities"]
        # needed because sometimes the values returned from ParameterSet are np.float, which yaml does not like
        for k, v in improved_parameter_dict.items():
            if isinstance(v, np.floating):
                improved_parameter_dict[k] = float(v)

        if improved_parameter_dict[PLATFORM_COHERENCE[hardware_platform]] > 0:
            improved_parameter_dict["cutoff_time"] = \
                0.65 * improved_parameter_dict[PLATFORM_COHERENCE[hardware_platform]]
        else:
            improved_parameter_dict["cutoff_time"] = None
        # dump to yaml
        param_file = param_file.split('.yaml')[0] + '_improvement_factor_' + str(improvement_factor) + '.yaml'

        with open(param_file, "w") as stream:
            yaml.dump(improved_parameter_dict, stream)

        return param_file
    else:
        return param_file


def _update_config_file_with_bright_state_params(config_file, param_file):
    """Updates the bright state parameters of the configuration file, ensuring that the product of transmittance and
    bright state (and hence overall photon detection probability) remains constant across different node - heralding
    station links.

    Parameters
    ----------
    config_file : str
        Name of configuration file.
    param_file : str
        Name of parameter file.
    """

    with open(param_file, "r") as stream:
        sim_params = yaml.load(stream, Loader=Loader)
    bright_state_param = sim_params["bright_state_param"]

    fancy_yaml = YAML()
    with open(config_file, 'r') as stream:
        code = fancy_yaml.load(stream)
    transmittances = {}
    for connection in ["1", "2"]:
        connection_props = code["components"]["ent_connection_" + connection]["properties"]
        for side in ["A", "B"]:
            connection_loss = connection_props["length_" + side] * connection_props["p_loss_length_" + side]
            transmittances[connection + side] = np.power(10, -1 * connection_loss / 10)
    bright_state_params = {}
    bright_state_params["1A"] = float(bright_state_param)
    for key, val in transmittances.items():
        bright_state_params[key] = float(transmittances["1A"] / val * bright_state_params["1A"])
    code["components"]["start_node"]["properties"]["bright_state_param"] = {"ENT_B": bright_state_params["1A"]}
    code["components"]["repeater_1"]["properties"]["bright_state_param"] = {"ENT_A": bright_state_params["1B"],
                                                                            "ENT_B": bright_state_params["2A"]}
    code["components"]["end_node"]["properties"]["bright_state_param"] = {"ENT_A": bright_state_params["2B"]}
    with open(config_file, "w") as stream:
        fancy_yaml.dump(code, stream)


def determine_absolute_minimal_requirement(n_runs, tunable_param_name, tunable_param_values, param_file, param_name,
                                           config_file, improvement_factor, rate_target, server_coherence_time=100):
    """Determines absolute minimal requirement on a parameter for a given rate target and assumption on server coherence
    time.

    Sweeps over values of given tunable parameter, running simulation for each. If any of the values attains the target
    metric, the function breaks and the identified minimal requirement is written to a file.
    If none of the values are enough, `None` and `None` are returned.

    Parameters
    ----------
    n_runs : int
        How many simulation runs per tunable parameter value.
    tunable_param_name : str or None
        Name of tunable parameter. If `None`, simulation is run for only one data point.
    tunable_param_values : list of ints or None
        Values of tunable parameter. If `None`, simulation is run for only one data point.
        Else, it is run for all values given.
    param_file : str
        Name of parameter file.
    param_name : str
        Name of parameter being improved.
    config_file : str
        Name of configuration file.
    improvement_factor : float
        Factor with which parameter was improved.
    rate_target : float
        Target entanglement generation rate (Hz).
    server_coherence_time : float
        Coherence time of server qubits (s).
    """
    fidelity_target = 0.5 * (1 + 1 / np.sqrt(2) * np.exp(1 / (2 * server_coherence_time * rate_target)))
    if tunable_param_values is None:
        tunable_param_values = [0]
    for tunable_param_value in tunable_param_values:
        with open(param_file, "r") as stream:
            sim_params = yaml.load(stream, Loader=Loader)
        if tunable_param_name is not None:
            sim_params[tunable_param_name] = tunable_param_value

        # dump to yaml to be picked up by netconf
        with open(param_file, "w") as stream:
            yaml.dump(sim_params, stream)

        if tunable_param_name == "bright_state_param":
            _update_config_file_with_bright_state_params(config_file, param_file)

        generator, ae, number_nodes, sim_params = setup_simulation(config_file=config_file,
                                                                   param_file=new_param_file)

        sim_params["improvement_factor"] = improvement_factor
        rdfh = collect_state_data(generator=generator, ae=ae, n_runs=n_runs, sim_params=sim_params,
                                  varied_param=None, varied_object=None, number_nodes=number_nodes)

        av_teleportation_fidelity, _ = estimate_average_teleportation_fidelity_from_data(
            determine_teleportation_fidelities_of_xyz_eigenstates_from_data(rdfh.dataframe))
        av_rate = 1 / estimate_duration_per_success(rdfh.dataframe)[0]
        print("Average rate is {} and average teleportation fidelity is {}".format(av_rate, av_teleportation_fidelity))
        if av_rate > rate_target and av_teleportation_fidelity > fidelity_target:
            print("improvement factor {} for parameter {} enough".format(improvement_factor, param_name))
            with open("minimal_requirements.csv", "a") as file:
                writer = csv.writer(file)
                writer.writerow([param_name, improvement_factor,
                                 sim_params[param_name], av_rate, av_teleportation_fidelity])
            return av_rate, av_teleportation_fidelity
    return None, None


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument('configfile', type=str, help="Name of the config file.")
    parser.add_argument('-bpf', '--baselineparamfile', required=True, type=str,
                        help="Name of the baseline parameter file.")
    parser.add_argument('-ppf', '--perfectparamfile', required=True, type=str,
                        help="Name of the perfect parameter file.")
    parser.add_argument('-pn', '--param_name', required=True, type=str,
                        help="Name of parameter on which sensitivity analysis will be performed.")
    parser.add_argument('-pltf', '--platform', required=True, type=str,
                        help="Hardware platform. Abstract, ae, nv or ti.")
    parser.add_argument('-if', '--improvement_factor_limits', nargs='+', required=True, type=float,
                        help="These define the range over which to sweep.")
    parser.add_argument('-ifs', '--improvement_factor_step', required=False, type=float, default=0.1,
                        help="Size of step in improvement factor step.")
    parser.add_argument('-tpn', '--tunable_param_name', required=False, type=str, default=None,
                        help="Name of tunable parameter over which to sweep.")
    parser.add_argument('-tpv', '--tunable_param_values', nargs='+', required=False, type=float, default=None,
                        help="Values of tunable parameter over which to sweep.")
    parser.add_argument('-n', '--n_runs', required=False, type=int, default=100,
                        help="Number of runs per configuration. Defaults to 100.")
    parser.add_argument('-rt', '--rate_target', required=True, type=float,
                        help="Rate (Hz) target.")
    parser.add_argument('-ct', '--server_coherence_time', required=False, type=float,
                        help="Server coherence time (s). Defaults to 100 s. Used for computing fidelity target.")
    parser.add_argument('--output_path', required=False, type=str, default="raw_data",
                        help="Path relative to local directory where simulation results should be saved.")

    args, unknown = parser.parse_known_args()
    baseline_sim_params = read_from_file(file_name=args.baselineparamfile)
    perfect_sim_params = read_from_file(file_name=args.perfectparamfile)

    perfect_sim_params[args.param_name] = baseline_sim_params[args.param_name]
    imperfect_param_file = args.perfectparamfile.split(".yaml")[0] + "_" + args.param_name + ".yaml"
    dump_to_file(file_name=imperfect_param_file,
                 params=perfect_sim_params)

    update_config_file(config_file=args.configfile,
                       param_file=imperfect_param_file,
                       platform=args.platform)
    start_if = args.improvement_factor_limits[0]
    end_if = args.improvement_factor_limits[1]
    for improvement_factor in np.arange(start_if, end_if, args.improvement_factor_step):
        # due to rounding errors, have to do this in ad-hoc manner
        if args.param_name == "coherence_time":
            perfect_sim_params["coherence_time"] *= float(np.sqrt(improvement_factor))
            perfect_sim_params["cutoff_time"] = 0.65 * perfect_sim_params["coherence_time"]
            new_param_file = \
                imperfect_param_file.split('.yaml')[0] + '_improvement_factor_' + str(improvement_factor) + '.yaml'
            with open(new_param_file, "w") as stream:
                yaml.dump(perfect_sim_params, stream)
        else:
            new_param_file = improve_parameter_file(param_file=imperfect_param_file,
                                                    improvement_factor=improvement_factor,
                                                    param_to_improve=args.param_name,
                                                    hardware_platform=args.platform,
                                                    baseline_sim_params=perfect_sim_params)

        update_config_file(config_file=args.configfile,
                           param_file=new_param_file,
                           platform=args.platform)
        print("Running simulation improving {} by a factor of {}".format(args.param_name, improvement_factor))
        av_rate, av_teleportation_fidelity = determine_absolute_minimal_requirement(
            tunable_param_name=args.tunable_param_name,
            tunable_param_values=args.tunable_param_values,
            config_file=args.configfile,
            param_file=new_param_file,
            param_name=args.param_name,
            n_runs=args.n_runs,
            improvement_factor=improvement_factor,
            rate_target=args.rate_target)
        if av_rate:
            break
    print('')
