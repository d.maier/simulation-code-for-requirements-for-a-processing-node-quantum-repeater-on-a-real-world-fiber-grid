# !/bin/bash

parameter_file_name="nv_parameters.txt"
while read parameter
do
    echo "Performing absolute minimal requirement analysis for parameter $parameter"
    python find_absolute_minimal_requirements.py nv_surf_config.yaml -bpf nv_baseline_params.yaml -ppf nv_perfect_params.yaml \
    -pn $parameter -pltf nv -if 1 5 -ifs 1 -n 100 -rt 0.1
done < "$parameter_file_name"
