import csv
import yaml
import time
import matplotlib
import numpy as np
import matplotlib.pyplot as plt
from argparse import ArgumentParser
from simulations.unified_simulation_script_state import run_unified_simulation_state
from netsquid_netconf.netconf import Loader

font = {'size': 16}

matplotlib.rc('font', **font)


def update_cutoff_time(param_file, cutoff_time):
    # load sim parameters from file
    with open(param_file, "r") as stream:
        sim_params = yaml.load(stream, Loader=Loader)

    sim_params["cutoff_time"] = float(cutoff_time)

    # dump to yaml to be picked up by netconf
    with open(args.paramfile, "w") as stream:
        yaml.dump(sim_params, stream)


def plot_runtime_data(runtime_dict, filename):
    run_arr = np.array(list(runtime_dict.keys()))
    runtime_list = list(runtime_dict.values())
    runtime_av_arr = np.array([runtime_list[i][0] for i in range(len(runtime_list))])
    runtime_std_arr = np.array([runtime_list[i][1] for i in range(len(runtime_list))])

    plt.errorbar(run_arr, runtime_av_arr, yerr=runtime_std_arr / np.sqrt(len(run_arr)),
                 fmt='o', label="Measured runtime (s)", markersize=1)
    plt.legend()
    plt.grid(True)
    plt.xlabel("Cut-off (ns)")
    plt.ylabel("Runtime (s)")

    plt.savefig(filename + '.pdf', dpi=300, bbox_inches='tight')
    plt.savefig(filename + '.png', dpi=300, bbox_inches='tight')


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument('configfile', type=str, help="Name of the config file.")
    parser.add_argument('-pf', '--paramfile', required=False, type=str, help="Name of the parameter file.")
    parser.add_argument('-n', '--n_runs', type=int, default=20, required=True, help="Number of runs.")
    parser.add_argument('--cutoffs', nargs="+", required=True,
                        help="Values of the cut-off time for which to run the analysis.")
    parser.add_argument('--output_file_name', type=str, required=False, default="runtime_analysis",
                        help="Name of file number of runs and run time.")

    args, unknown = parser.parse_known_args()
    elapsed_time_per_number_runs = {}
    number_of_repetitions_per_data_point = 10
    for cutoff in args.cutoffs:
        elapsed_time = []
        for number_of_reps in range(number_of_repetitions_per_data_point):
            update_cutoff_time(args.paramfile, cutoff)
            start_time = time.time()
            run_unified_simulation_state(configfile=args.configfile,
                                         paramfile=args.paramfile,
                                         n_runs=args.n_runs)
            elapsed_time_one_run = time.time() - start_time
            elapsed_time.append(elapsed_time_one_run)
        elapsed_time_av = np.average(elapsed_time)
        elapsed_time_std = np.std(elapsed_time)
        elapsed_time_per_number_runs[float(cutoff)] = [elapsed_time_av, elapsed_time_std]

    timestr = time.strftime("%Y%m%d_%H%M%S")
    filename = args.output_file_name + '_' + timestr

    with open(filename + '.csv', 'w') as csv_file:
        writer = csv.writer(csv_file)
        writer.writerow(["cutoff", "elapsed_time_av(s)", "elapsed_time_std(s)"])
        for key, val in elapsed_time_per_number_runs.items():
            writer.writerow([key, val])

    plot_runtime_data(runtime_dict=elapsed_time_per_number_runs,
                      filename=filename)
